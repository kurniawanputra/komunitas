import React,{Component} from 'react';
import Footer from './../../components/Footer/Footer';
import Menu from './../../components/Menu/Menu';
import Saran from './../../components/Saran/AddSaran';

export default class Hubungi extends Component {
  render()
  {
    return(
      <div>
        <Menu />
        <Saran />
        <Footer />
      </div>
    );
  }
}