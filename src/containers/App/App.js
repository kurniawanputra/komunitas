import React, { Component } from 'react'
import { Switch, Route, withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import NotFound from './../NotFound/NotFound';
import AlreadyLogin from './../NotFound/AlreadyLogin';
import TheRoutes from 'utils/TheRoutes';

const PropsRoute = ({ authType, component: Component, initialData, ...rest }) => {
  const user = localStorage.getItem('token');
  if (authType === 'loggedin') {
    if (!user) {
      return (
        <Route {...rest} render={props => (
          <NotFound {...props} />
        )} />
      )
    }
  } else if(authType === 'notloggedin') {
    if (user) { 
      return (
        <Route {...rest} render={props => (
          <AlreadyLogin {...props} />
        )} />
      )
    }
  }
  return (
    <Route {...rest} render={props => (
      <Component {...props} initialData={initialData} />
    )} />
  )
}

class App extends Component {
  render() {
    return (
      <Switch>
        {TheRoutes.map((route, key) => {
          return (
            <PropsRoute {...route} key={key} initialData={this.props.initialData} />
          )
        })}
      </Switch>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user
})

export default withRouter(connect(mapStateToProps)(App));