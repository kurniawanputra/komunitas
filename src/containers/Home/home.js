import React, {Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import Sejarah from './../../components/Sejarah/Sejarah';
import Gallery from './../../components/Gallery/Sliding';
import Foto from './../../components/Gallery/GalleryHome';
import BeritaHome from './../../components/Berita/BeritaHome';
import Visi from './../../components/Sejarah/visi';

export default class home extends Component {
  render() {
    return(
      <div>
        <Menu />
        <Sejarah />
        <Foto />
        <Visi />
        <BeritaHome />
        <Footer />
      </div>
    )
  }
}