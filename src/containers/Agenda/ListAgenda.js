import React, {Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import List from './../../components/List/FormListAgenda';


export default class ListAgenda extends Component {
  render () {
    return(
      <div>
        <Menu />
        <List />
        <Footer />
      </div>
    );
  }
}