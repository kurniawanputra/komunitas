import React, { Component } from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import FormUpdateAgenda from './../../components/Agenda/FormUpdateAgenda';

export default class UpdateAgenda extends Component {
  render() {
    return(
      <div>
        <Menu />
        <FormUpdateAgenda />
        <Footer />
      </div>
    )
  }
}