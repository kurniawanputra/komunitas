import React, {Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import FormAddAgenda from './../../components/Form/FormAddAgenda';

export default class AddAgenda extends Component {
  render() {
    return(
      <div>
        <Menu />
        <FormAddAgenda />
        <Footer />
      </div>
    )
  }
}