import React, {Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import Event from './../../components/Event/JadwalEvent';

export default class Agenda extends Component {
  render() {
    return(
      <div>
        <Menu />
        <Event />
        <Footer />
      </div>
    );
  }
}