import React,{Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import BeritaDetail from './../../components/Berita/BeritaDetail';

export default class FormDetailBerita extends Component {
  render() {
    return(
      <div>
        <Menu />
        <BeritaDetail />
        <Footer />
      </div>
    )
  }
}