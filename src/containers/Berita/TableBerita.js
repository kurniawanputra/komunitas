import React, { Component } from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import ListBerita from './../../components/Berita/AllListBerita';

export default class TableBerita extends Component {
  render() {
    return (
      <div>
        <Menu />
        <ListBerita />
        <Footer />
      </div>
    )
  }
}