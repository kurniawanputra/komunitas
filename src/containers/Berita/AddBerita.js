import React, { Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import FormAddBerita from './../../components/Form/FormAddBerita';

export default class AddBerita extends Component {
  render() {
    return(
      <div>
        <Menu />
        <FormAddBerita />
        <Footer />
      </div>
    );
  }
}