import React, {Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import ListBerita from './../../components/Berita/ListBerita';

export default class Berita extends Component {
  render() {
    return(
      <div>
        <Menu />
        <ListBerita />
        <Footer />
      </div>
    );
  }
}