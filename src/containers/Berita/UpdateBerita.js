import React, {Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import FormUpdateBerita from './../../components/Berita/FormUpdateBerita';

export default class Update extends Component {
  render() {
    return(
      <div>
        <Menu />
        <FormUpdateBerita />
        <Footer />
      </div>
    )
  }
}