import React, {Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import Sejarah from './../../components/Sejarah/Sejarah';
import ListFoto from './../../components/Gallery/ListFoto';
import Pengurus from './../../components/Profil/Pengurus';

export default class Profil extends Component {
  render() {
    return(
      <div>
        <Menu />
        <Sejarah />
        <Pengurus />
        <Footer />
      </div>
    );
  }
}