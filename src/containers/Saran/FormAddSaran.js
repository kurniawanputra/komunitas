import React, {Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import AddSaran from './../../components/Saran/AddSaran';

export default class FormAddSaran extends Component {
  render() {
    return(
      <div>
        <Menu />
        <AddSaran />
        <Footer />
      </div>
    )
  }
}