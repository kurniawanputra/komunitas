import React, { Component } from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import AddGallery from './../../components/Form/FormAddGallery';

export default class FormInputGallery extends Component {
  render() {
    return(
      <div>
        <Menu />
        <AddGallery />
        <Footer />
      </div>
    )
  }
}