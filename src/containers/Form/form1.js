import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from 'redux-modules/modules/user';
// import * as userActions from './../../redux/modules/user';
import { Link } from 'react-router-dom';
import CardJson from './../../JSON/cards';
import ListUang from './../../JSON/listNama';
import { withStyles } from 'material-ui/styles';
import MenuItem from 'material-ui/Menu/MenuItem';
import TextField from 'material-ui/TextField';
import green from 'material-ui/colors/green';
import Button from 'material-ui/Button';
import classNames from 'classnames';
import { CircularProgress } from 'material-ui/Progress';



const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  wrapper: {
    margin: theme.spacing.unit,
    position: 'relative',
  },
  buttonSuccess: {
    backgroundColor: green[500],'&:hover': {backgroundColor: green[700],},
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
});

class form1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name : 'silahkan isi nama anda',
      alamat : 'silahkan isi alamat anda',
      uang : 'silahkan pilih mata uang anda',
      success : false,
      loading : false
    };
    
  }
  componentWillMount() {
    
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  timer = undefined;

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  handleButtonSave = () => {
    if (!this.state.loading) {
      this.setState(
        {
          success: false,
          loading: true,
        },
        () => {
          this.timer = setTimeout(() => {this.setState({loading: false,success: true,});}, 2000);
        },
      );
    }
  };


  

  render() {
    const { success, loading } = this.state;
    const { classes } = this.props;
    const buttonClassname = classNames({
      [classes.buttonSuccess]: success,
    });

    return (
      <div>
        <form>
        <h2>Second Page</h2>
        <TextField
          id="name"
          label="Name"
          className={classes.textField}
          value={this.state.name}
          onChange={this.handleChange('name')}
          margin="normal"
        />
        <br />
        <TextField
          id="alamat"
          label="Alamat"
          className={classes.textField}
          value={this.state.alamat}
          onChange={this.handleChange('alamat')}
          margin="normal"
        />
        <br />
        <TextField
          id="piliih mata uang"
          select
          label="Select"
          value = {this.state.uang}
          className={classes.textField}
          onChange={this.handleChange('uang')}
          SelectProps={{
            MenuProps: {
              className: classes.menu,
            },
          }}
          helperText="Please select your currency"
          margin="normal"
          >
          {ListUang.map(option => (
            <MenuItem key={option.uang} value={option.type}>
              {option.type}
            </MenuItem>
          ))}
        </TextField>  

        <div className={classes.wrapper}>
          <Button
            raised
            color="primary"
            className={buttonClassname}
            disabled={loading}
            onClick={this.handleButtonSave}
            >
            Save
          </Button>
          {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
        </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user
})

const mapDispatchToProps = dispatch => ({
  userActions: bindActionCreators(userActions, dispatch)
})

export default withStyles(styles) (form1)
