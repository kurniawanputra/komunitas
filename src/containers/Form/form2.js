import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'react-bootstrap';
import * as userActions from 'redux-modules/modules/user';
// import * as userActions from './../../redux/modules/user';
import { Link } from 'react-router-dom';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import Sejarah from './../../components/Sejarah/Sejarah';
import Gallery from './../../components/Gallery/Sliding';
import ListBerita from './../../components/Berita/ListBerita';


class Form2 extends Component {
  componentWillMount() {

  }


  render() {
    return (
      <div>
          <Menu />
          <Link to={'/'}>First</Link>
          <Gallery />
          <Sejarah />
          <ListBerita />
          <Footer />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
})

const mapDispatchToProps = dispatch => ({
  userActions: bindActionCreators(userActions, dispatch)
})

export default (Form2)
