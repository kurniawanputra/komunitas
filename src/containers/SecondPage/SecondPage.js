import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'react-bootstrap';
import * as userActions from 'redux-modules/modules/user';
// import * as userActions from './../../redux/modules/user';
import { Link } from 'react-router-dom';
import './SecondPage.css';
import { withStyles } from 'material-ui/styles';
import CardJson from './../../JSON/cards';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';


const styles = {
  card: {
    maxWidth: 345,
  },
  media: {
    height: 200,
  },
};

class SecondPage extends Component {
  componentWillMount() {
    // const payload = {
    //   userId: 1,
    //   id: 1,
    //   title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    //   body: "quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto"
    // }
    // this.props.userActions.set(payload);
    //this.props.userActions.tes();
  }
  

  render() {
    const { classes } = this.props;
    return (
      <div>
        <h2>Second Page</h2>
        <Link to={'/'}>First</Link>
        <Table striped bordered condensed hover>
          <thead>
            <tr>
              <th>Nomor</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Agung</td>
              <td>Gambir</td>
              <td></td>
            </tr>
            <tr>
              <td>2</td>
              <td>Anung</td>
              <td>Cempaka putih</td>
              <td></td>
            </tr>
            <tr>
              <td>3</td>
              <td>Andong</td>
              <td>Cempaka mas</td>
              <td></td>
            </tr>
            <tr>
              <td>4</td>
              <td>Andri</td>
              <td>Rasuna Said</td>
              <td></td>
            </tr>
          </tbody>
        </Table>
        <div>
          {CardJson.map((card, key) =>
            <Card className={classes.card}>
              <CardMedia
              className={classes.media}
                image={require(`./../../image/${card.gambar}`)}
                title={card.deskripsi}
              />
              <CardContent>
                <Typography type="headline" component="h2">
                  Lizard - {card.gambar}
              </Typography>
                <Typography component="p">
                  {card.isi}  
                </Typography>
              </CardContent>
              <CardActions>
                <Button dense color="primary">
                  Share
              </Button>
                <Button dense color="primary">
                  Learn More
              </Button>
              </CardActions>
            </Card>
          )
        }
        </div>


      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user
})

const mapDispatchToProps = dispatch => ({
  userActions: bindActionCreators(userActions, dispatch)
})

export default withStyles(styles)(SecondPage)
