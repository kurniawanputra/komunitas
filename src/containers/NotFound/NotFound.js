import React, { Component } from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';

export default class NotFound extends Component {
  render() {
    return(
      <div>
        <Menu />
        <h2>Halaman ini hanya bisa di akses oleh admin</h2>
        <Footer />
      </div>
    )
  }
}

// export default class NotFound extends Component {
//   render() {
//     return (<NoMatch />);
//   }
// }