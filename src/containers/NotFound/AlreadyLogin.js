import React, { Component } from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';

export default class AlreadyLogin extends Component {
  render() {
    return(
      <div>
        <Menu />
        <h2>Maaf anda sudah login</h2>
        <Footer />
      </div>
    )
  }
}