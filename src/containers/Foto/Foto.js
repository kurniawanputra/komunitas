import React, {Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import ListFoto from './../../components/Gallery/ListFoto';

export default class Foto extends Component {
  render() {
    return(
      <div>
        <Menu />
        <ListFoto />
        <Footer />
      </div>
    );
  }
}