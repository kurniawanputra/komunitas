import React, { Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import Login from './../../components/User/CekLogin';

export default class FormLogin extends Component {
  render() {
    return(
      <div>
        <Menu />
        <Login />
        <Footer />
      </div>
    )
  }
}