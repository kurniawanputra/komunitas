import React, { Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import UpdateUser from './../../components/User/UpdateUser';

export default class FormUpdateUser extends Component {
  render() {
    return(
      <div>
        <Menu />
        <UpdateUser />
        <Footer />
      </div>
    )
  }
}