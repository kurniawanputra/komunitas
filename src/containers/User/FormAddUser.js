import React, {Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import AddUser from  './../../components/User/CreateUser';

export default class FormAddUser extends Component {
  render() {
    return(
      <div>
        <Menu />
        <AddUser />
        <Footer />
      </div>
    )
  }
}