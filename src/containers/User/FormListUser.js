import React, {Component} from 'react';
import Menu from './../../components/Menu/Menu';
import Footer from './../../components/Footer/Footer';
import ListUser from './../../components/User/ListUser';

export default class FormListUser extends Component {
  render() {
    return(
      <div>
        <Menu />
        <ListUser />
        <Footer />
      </div>
    )
  }
}