import React, {Component } from 'react';
import Slider from 'react-slick';
import CardJson from './../../JSON/cards';

export default class Sliding extends Component {
  render() {
    const { classes } = this.props;
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000
    };
    return(
      <div>
        <h2>Gallery</h2>
        {
            <Slider {...settings}>
              {CardJson.map((card, key) =>
                <div><img src={require(`./../../image/${card.gambar}`)} width={600} height={350} style={{ display: 'block', marginLeft: 'auto', marginRight: 'auto', marginTop: '10px' }} /></div>
              )
              }
            </Slider>
          }
      </div>
    );
  }
}