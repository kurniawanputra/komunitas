import React, { Component, PropTypes } from 'react';
import { PhotoSwipeGallery } from 'react-photoswipe-component';
import Logo from './../../image/logo.jpg';
import PhotoSwipe from './PhotoSwipe';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as galleryActions from 'redux-modules/modules/gallery';
import './../../global-styles/photoswipe.css';
import './../../global-styles/default-skin.css';
import './../../global-styles/style.css';

class ListFoto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gallery: { response: { data: [] } },
      LoadMore: false
    };

    this.handleLoadmore = this.handleLoadmore.bind(this);
  }

  handleLoadmore() {
    const { gallery } = this.state;
    let page = 1;
    if (gallery.loaded) {
      if (gallery.response) {
        page = gallery.response.page + 1;
        if (gallery.response.data.length === gallery.response.total) {
          this.setState({ LoadMore: false})
        } else {
          this.setState({ LoadMore: true})
        }
      } 
    }
    this.props.galleryActions.ListGallery(page);
  }

  componentWillMount() {
    this.props.galleryActions.ListGallery(1);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.gallery && nextProps.gallery.response) {
      console.log(nextProps.gallery);
      console.log('nextgallery', nextProps.gallery.response.data);
      const { gallery } = nextProps;
      let response = { loaded: false, loading: true, data: [] };
      if (gallery.loaded) {
        if (gallery.response) {
          response = {
            ...gallery.response,
            data: [
              ...this.state.gallery.response.data,
              ...nextProps.gallery.response.data
            ]
          }
        }
      }
      console.log('response', response);
      this.setState({
        LoadMore: response.data.length < response.total,
        gallery: {
          ...gallery,
          response
        }
      })
    }
  }


  render() {
    const items = this.state.gallery.response.data.map(gallery => ({ src: `${process.env.REACT_APP_APIHOST}/photo/${gallery.url_gallery}`, w: 650, h: 650, caption: gallery.judul_gallery }));
    console.log('items', items);

    return (
      <div className="container">
        <h2>Gallery foto</h2>
        <PhotoSwipe gallery={this.state.gallery} items={items} LoadMore={this.state.LoadMore} handleLoadmore={this.handleLoadmore}/>
      </div>
    );
  }
}

ListFoto.propTypes = {
  gallery: PropTypes.object,
  galleryActions: PropTypes.func.isRequired
}

ListFoto.contexttypes = {
  router: React.PropTypes.object.isRequired
}

function MapStateToProps(state) {
  return {
    gallery: state.gallery
  }
}

const MapDispatchToProps = dispatch => ({
  galleryActions: bindActionCreators(galleryActions, dispatch)
})


export default connect(MapStateToProps, MapDispatchToProps)(ListFoto);