import React, {Component, PropTypes} from 'react';
import * as fotohomeActions from 'redux-modules/modules/fotohome';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PhotoSwipeHome from './PhotoSwipeHome';
import './../../global-styles/photoswipe.css';
import './../../global-styles/default-skin.css';
import './../../global-styles/style.css';



class GalleryHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fotohome: { response: { data: [] } }
    }

    this.handleLengkap = this.handleLengkap.bind(this);
  }

  handleLengkap() {
    window.location.href = `${process.env.REACT_APP_HOST}/foto`;
  }

  componentWillMount() {
    this.props.fotohomeActions.FotoHome();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.fotohome && nextProps.fotohome.response && nextProps.fotohome.response.data) {
      console.log('fotohome', nextProps.fotohome);
      this.setState({ fotohome: nextProps.fotohome })
    }
  }

  render() {
    const items = this.state.fotohome.response.data.map(fotohome => ({ src: `${process.env.REACT_APP_APIHOST}/photo/${fotohome.url_gallery}`, w: 700, h: 700, caption: fotohome.judul_gallery }));
    console.log('items', items);
    return(
      <div style={{marginBottom: '10px'}}>
        <PhotoSwipeHome fotohome={this.state.fotohome} items={items} />
        <button type="button" className="btn btn-primary" style={{marginTop: '10px'}} 
        onClick={this.handleLengkap}>Lihat Selengkapnya</button>
      </div>
    )
  }
}

GalleryHome.propTypes = {
  fotohome: PropTypes.object,
  fotohomeActions: PropTypes.func.isRequired
}

GalleryHome.contextTypes = {
  router: React.PropTypes.object.isRequired
}

function MapStateToProps(state) {
  return {
    fotohome: state.fotohome
  }
}

const MapDispatchToProps = dispatch => ({
  fotohomeActions: bindActionCreators(fotohomeActions, dispatch)
})

export default connect(MapStateToProps, MapDispatchToProps)(GalleryHome);