import React, { Component, PropTypes } from 'react';
import { PhotoSwipeGallery } from 'react-photoswipe-component';
import Logo from './../../image/logo.jpg';
import './../../global-styles/photoswipe.css';
import './../../global-styles/default-skin.css';
import './../../global-styles/style.css';

export default class PhotoSwipe extends Component {
  render() {
    const { gallery, items, LoadMore, handleLoadmore} = this.props;
    console.log('photo swipe', gallery);
    return (
      <div className="container">
        {gallery.response && gallery.response.data && gallery.response.data.length === 0 &&
          <h1>Maaf tidak ada foto</h1>
        }
        {gallery.response && gallery.response.data && gallery.response.data.length > 0
          &&
          <PhotoSwipeGallery items={items} options={{ history: false }} />
        }

        <div>
          {LoadMore && <button className="btn btn-primary" onClick={handleLoadmore}>load more</button>}
        </div>
      </div>
    );
  }
}