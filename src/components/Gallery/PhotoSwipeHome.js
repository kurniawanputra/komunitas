import React, { Component, PropTypes } from 'react';
import { PhotoSwipeGallery } from 'react-photoswipe-component';
import Logo from './../../image/logo.jpg';
import './../../global-styles/photoswipe.css';
import './../../global-styles/default-skin.css';
import './../../global-styles/style.css';

export default class PhotoSwipeHome extends Component {
  render() {
    const { fotohome, items} = this.props;
    return(
      <div className="container">
        {fotohome.response && fotohome.response.data && fotohome.response.data.length === 0 &&
          <h1>Maaf tidak ada foto</h1>
        }
        {fotohome.response && fotohome.response.data && fotohome.response.data.length > 0
          &&
          <PhotoSwipeGallery items={items} options={{ history: false }} />
        }
      </div>
    );
  }
}