import React, { Component, PropTypes } from 'react';
import { withRouter } from 'react-router-dom';
import ListEvents from './../../JSON/listEvent';
import BigCalendar from 'react-big-calendar';
import moment, { locale } from 'moment';
import * as agendaActions from 'redux-modules/modules/agenda';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './../../global-styles/react-big-calendar.css';
import './../../global-styles/react-datetime.css';

BigCalendar.momentLocalizer(moment);

const Events = [
  {
    'title': 'All Day Event very long title',
    'allDay': true,
    'start': "Sat Dec 09 2017 03:00:00 GMT+0700 (SE Asia Standard Time)",
    // 'start': "Wed Nov 08 2015 21:00:00 GMT+0700 (SE Asia Standard Time)",
    'end': "Sat Dec 09 2017 03:00:00 GMT+0700 (SE Asia Standard Time)"
  }
];

class JadwalEvent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      agenda: { response: { data: [] } }
    }
    //this.handleSlot = this.handleSlot.bind(this);
    //this.handleData = this.handleData.bind(this);
  }
  componentWillMount() {
    this.props.agendaActions.AllAgenda();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.agenda && nextProps.agenda.response && nextProps.agenda.response.data) {
      console.log('agenda', nextProps.agenda);
      nextProps.agenda.response.data = nextProps.agenda.response.data.map(agenda => {
        return {
          title: agenda.title,
          allDay: true,
          start: new Date(agenda.start_date).toString(),
          end: new Date(agenda.end_date).toString()
        }
      });
      console.log('nextAgenda', nextProps.agenda.response.data);
      this.setState({ agenda: nextProps.agenda })
    }
  }
  render() {
    console.log('data', this.state.agenda.response.data);
    return (
      <div>
        <h2>Jadwal Agenda</h2>
        <BigCalendar
          selectable
          events={this.state.agenda.response.data}
          defaultView='month'
          scrollToTime={new Date(1980, 1, 1, 6)}
          defaultDate={new Date(2017, 11, 0)}
          //defaultDate={new Date(2017, 10, 8)}
          style={{ height: 800 }}
          onSelectEvent={agenda => alert(agenda.title)}
          onSelectSlot={(slotInfo) => alert(
            `selected slot: \n\nstart ${slotInfo.start.toLocaleString()} ` +
            `\nend: ${slotInfo.end.toLocaleString()}` +
            `\naction: ${slotInfo.action}`
          )}
        />
      </div>
    )
  }
}

JadwalEvent.propTypes = {
  agenda: PropTypes.object,
  agendaActions: PropTypes.func.isRequired
}

JadwalEvent.contextTypes = {
  router: React.PropTypes.object.isRequired
}

function MapStateToProps(state) {
  return {
    agenda: state.agenda
  }
}

const MapDispatchToProps = dispatch => ({
  agendaActions: bindActionCreators(agendaActions, dispatch)
})

export default withRouter(connect(MapStateToProps, MapDispatchToProps)(JadwalEvent));