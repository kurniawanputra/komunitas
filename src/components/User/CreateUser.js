import React, { Component } from 'react';
import { CircularProgress } from 'material-ui/Progress';
import Button from 'material-ui/Button';
import green from 'material-ui/colors/green';
import Save from 'material-ui-icons/Save';
import Cancel from 'material-ui-icons/Cancel';
import { Alert } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { AddUser } from './../../common/userActions';

class CreateUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      loading: false,
      success: false,
      alertError: false
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleError  = this.handleError.bind(this);
  }

  handleError() {
    this.setState({
      alertError : false
    });
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  timer = undefined;

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.username === "" || this.state.password === "") {
      this.timer = setTimeout(() => this.setState({ loading: false }), 2000);
      this.setState({ alertError: true});
    } else {
      this.setState({ loading: true });
      const data = {
        username : this.state.username,
        password : this.state.password
      }
      this.props.AddUser(data, this.props.history.push);
    }
  }


  render() {
    let { loading, success, alertError } = this.state;
    return (
      <div className="container">
        {alertError && <Alert bsStyle="danger" onDismiss={this.handleError}>
          <h4>Mohon lengkapi username dan password</h4>
          </Alert>
        }
        <h2>Form Create User</h2>
        <form className="form-horizontal" enctype="multipart/form-data">
          <div className="form-group">
            <label className="control-label col-sm-2">Username</label>
            <div className="col-sm-10">
              <input type="text" name="username" value={this.state.username} onChange={this.handleChange} className="form-control" placeholder="Masukkan username"></input>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Password</label>
            <div className="col-sm-10">
              <input type="password" name="password" value={this.state.password} onChange={this.handleChange} className="form-control" placeholder="Masukkan password"></input>
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <Button raised color="primary" disabled={loading} onClick={this.handleSubmit}
                style={{
                  backgroundColor: '#86ec65',
                  '&:hover': 'backgroundColor: green[700]', marginRight: '10px'
                }}
              ><Save style={{ marginRight: 'theme.spacing.unit' }} />
                Save</Button>
              {loading && <CircularProgress size={24}
                style={{
                  color: 'green[500]', position: 'absolute', top: '50%',
                  left: '50%', marginTop: '-12', marginLeft: '-40'
                }} />}
              <Button raised color="default">
                <Cancel style={{ marginRight: 'theme.spacing.unit' }} />
                Clear
              </Button>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

CreateUser.propTypes = {
  AddUser: React.PropTypes.func.isRequired
}

CreateUser.contextTypes = {
  router: React.PropTypes.object.isRequired
}

export default withRouter(connect(null, { AddUser })(CreateUser));