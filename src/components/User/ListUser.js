import React, { Component, PropTypes } from 'react';
import { withRouter } from 'react-router-dom';
import { Table, Button, Pagination } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import ButtonMaterial from 'material-ui/Button';
import Delete from 'material-ui-icons/Delete';
import Edit from 'material-ui-icons/Edit';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
} from 'material-ui/Dialog';
import * as userActions from 'redux-modules/modules/listuser';
// const { DeleteUser } = userActions;
import * as userEditActions from 'redux-modules/modules/useredit';
// import { ActionDeleteUser } from './../../common/userActions';
//import axios from 'axios';
import request from 'superagent';
import Exclamation from './../../image/exclamation-mark.png';

class ListUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listuser: { response: { data: [] } },
      Page: 1,
      activePage: 1,
      open: false,
      selectedUserId: null
    }

    this.handleSelect  = this.handleSelect.bind(this);
    this.handleDelete  = this.handleDelete.bind(this);
    this.handleClose   = this.handleClose.bind(this);
    this.handleConfirm = this.handleConfirm.bind(this);
    this.addUser       = this.addUser.bind(this);
  }

  addUser() {
    window.location.href = `${process.env.REACT_APP_HOST}/adduser`;
  }

  handleDelete(selectedUserId) {
    this.setState({ open: true, selectedUserId });
  }

  handleConfirm(index) {
    //const id_user = this.state.selectedUserId;
    //var data = { id_user };
    const data = {
      id_user: this.state.selectedUserId
    }
    console.log("delete data", data);
    request.post(`${process.env.REACT_APP_APIHOST}/v1/deleteuser`)
      .send(data)
      .then(success => {
        this.props.userActions.DeleteUser(this.state.selectedUserId);
        this.setState({ open: false, selectedUserId: null });
      }, error => {
        this.setState({ open: false, selectedUserId: null });
      });

  }

  handleClose() {
    this.setState({ open: false, selectedUserId: null });
  }

  handleSelect(page) {
    this.props.userActions.ListUser(page);
  }

  componentWillMount() {
    this.props.userActions.ListUser(1);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.listuser && nextProps.listuser.response) {
      console.log(nextProps.listuser);
      console.log('list user', nextProps.listuser.response.data);
      this.setState({ listuser: nextProps.listuser })
    }
  }

  render() {
    const { fullScreen } = this.props;
    return (
      <div className="container">
        <div>
        <h2>List User</h2>
        <button type="button" className="btn btn-primary" style={{float: 'right', marginBottom: '10px'}} 
        onClick={this.addUser}>Tambah User</button>
        </div>
        <Table striped bordered condensed hover>
          <thead>
            <tr>
              <th style={{textAlign: "center"}}>No</th>
              <th style={{textAlign: "center"}}>Username</th>
              <th style={{textAlign: "center"}}>Edit</th>
              <th style={{textAlign: "center"}}>Delete</th>
            </tr>
          </thead>
          <tbody>
            {this.state.listuser.response && this.state.listuser.response.data && this.state.listuser.response.data.length === 0 &&
              <tr><td colSpan="7" style={{ textAlign: 'center' }}>Tidak ada Data</td></tr>
            }
            {this.state.listuser.response && this.state.listuser.response.data && this.state.listuser.response.data.length > 0
              && this.state.listuser.response.data.map((listuser, key) =>
                <tr key={listuser.id_user}>
                  <td>{((this.state.listuser.response.page - 1) * 10) + parseInt(key) + 1}</td>
                  <td>{listuser.username}</td>
                  <td onClick={event => {
                    this.props.userEditActions.setEditUser(listuser);
                    this.props.history.push(`/updateuser/${listuser.id_user}`);
                  }}
                  ><Edit /></td>
                  <td><ButtonMaterial raised color="default" onClick={() => this.handleDelete(listuser.id_user)}>
                    <Delete style={{ marginRight: 'theme.spacing.unit' }} />
                  </ButtonMaterial>
                  </td>
                </tr>
              )}
          </tbody>
          <Dialog
            fullScreen={fullScreen}
            open={this.state.open}
            onClose={this.handleClose}
            aria-labelledby="confirm delete">
            {/* <DialogTitle id="confirm delete">{"Hapus Data"}</DialogTitle> */}
            <DialogContent>
            <img src={Exclamation} width={200} height={200} />
            <br />
              <p style={{fontWeight: "bold"}}>
                Anda yakin mau hapus data?
              </p>
            </DialogContent>
            <div style={{ marginBottom: "10px", marginTop: "-20px"}}>
              <button onClick={this.handleConfirm} 
              style={{marginRight: "3px", padding: "8px", 
              backgroundColor: "#1a75ff", borderRadius: "5px", 
              color: "white", border: "none"}}>Ya, Hapus ini</button>
              <button onClick={this.handleClose} 
              style={{ padding: "8px", color: "white",
              backgroundColor: "#ff1a1a", borderRadius: "5px", border: "none"}}>Tidak</button>
            </div>
          </Dialog>
        </Table>
        <div>
          <div className="Paging" style={{ margin: 'auto', width: '17%' }}>
            <Pagination
              prev
              next
              first
              last
              ellipsis
              boundaryLinks
              items={this.state.listuser.response.total % 10 < 1 ? this.state.listuser.response.total / 10 : (this.state.listuser.response.total / 10) + 1}
              maxButtons={5}
              activePage={this.state.listuser.response.page}
              onSelect={this.handleSelect} />
          </div>
        </div>
      </div>
    )
  }
}

ListUser.propTypes = {
  listuser: PropTypes.object,
  userActions: PropTypes.func.isRequired
  //ActionDeleteUser: PropTypes.func.isRequired
}

ListUser.contextTypes = {
  router: React.PropTypes.object.isRequired
}

function MapStateToProps(state) {
  return {
    listuser: state.listuser
  }
}

const MapDispatchToProps = dispatch => ({
  userActions: bindActionCreators(userActions, dispatch),
  userEditActions: bindActionCreators(userEditActions, dispatch)
  // ActionDeleteUser: bindActionCreators(ActionDeleteUser, dispatch)
})

export default withRouter(connect(MapStateToProps, MapDispatchToProps)(ListUser));