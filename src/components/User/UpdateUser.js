import React, { Component, PropTypes } from 'react';
import * as usereditActions from 'redux-modules/modules/useredit';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { CircularProgress } from 'material-ui/Progress';
import Button from 'material-ui/Button';
import green from 'material-ui/colors/green';
import Save from 'material-ui-icons/Save';
import Cancel from 'material-ui-icons/Cancel';
import { Alert } from 'react-bootstrap';
import { ActionUpdateUser } from './../../common/userActions';

class UpdateUser extends Component {
  constructor(props) {
    super(props);
    let id_user = '';
    let username = '';
    if (props.useredit && props.useredit.loaded && props.useredit.response) {
      id_user = props.useredit.response.id_user;
      username = props.useredit.response.username;
    }
    this.state = {
      username,
      id_user,
      lama: '',
      baru: '',
      confirm: '',
      loaded: false,
      success: false,
      alertError: false,
      alertErrorSame: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAlertDismiss = this.handleAlertDismiss.bind(this);
    //this.handleAlertDismissSame = this.handleAlertDismissSame.bind(this);
  }

  handleAlertDismiss() {
    this.setState({ alertError: false, alertErrorSame: false});
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }
  
  handleSubmit(e) {
    e.preventDefault();
    if (this.state.username === "" || this.state.lama === "" || this.state.baru === "" || this.state.confirm === "") {
      this.timer = setTimeout(() => this.setState({ loading: false }), 2000);
      //alert("harap data di isi");
      this.setState({ alertError: true});
    } else if (this.state.lama == this.state.baru) {
      this.setState({ alertErrorSame: true});
    } else {
      this.setState({ loading: true });
      const data = {
        id_user: this.state.id_user,
        username: this.state.username,
        password: this.state.lama,
        baru: this.state.baru
      }
      this.props.ActionUpdateUser(data, this.props.history.push);
    }
  }

  timer = undefined;

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  componentWillMount() {
    if (!this.props.useredit.loaded) {
      this.props.usereditActions.loadEditUser(this.props.match.params.id);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.useredit && nextProps.useredit.loaded && nextProps.useredit.response) {
      console.log(nextProps.useredit);
      console.log('user edit', nextProps.useredit.response.data);
      this.setState({ useredit: nextProps.useredit })
    }
  }


  render() {
    let { loading, success, alertError, alertErrorSame } = this.state;
    return (
      <div className="container">
        {alertError && <Alert bsStyle="danger" onDismiss={this.handleAlertDismiss}>
          <h4>mohon lengkapi isi data</h4>
        </Alert>}
        {alertErrorSame && <Alert bsStyle="danger" onDismiss={this.handleAlertDismiss}>
          <h4>Maaf passowrd tidak boleh sama</h4>
        </Alert>}
        <h1>Form update user</h1>
        <form className="form-horizontal">
          <div className="form-group">
            <label className="control-label col-sm-2">id user</label>
            <div className="col-sm-10">
              <input type="text" disabled name="id_user" className="form-control" value={this.state.id_user}></input>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Username</label>
            <div className="col-sm-10">
              <input type="text" name="username" value={this.state.username} onChange={this.handleChange} className="form-control" placeholder="Masukkan username"></input>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Password Lama</label>
            <div className="col-sm-10">
              <input type="password" name="lama" value={this.state.lama} onChange={this.handleChange} className="form-control" placeholder="Masukkan password Lama"></input>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Password Baru</label>
            <div className="col-sm-10">
              <input type="password" name="baru" value={this.state.baru} onChange={this.handleChange} className="form-control" placeholder="Masukkan password Baru"></input>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Confirm Password Baru</label>
            <div className="col-sm-10">
              <input type="password" name="confirm" value={this.state.confirm} onChange={this.handleChange} className="form-control" placeholder="Ulangi password baru"></input>
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <Button raised color="primary" disabled={loading} onClick={this.handleSubmit}
                style={{
                  backgroundColor: '#86ec65',
                  '&:hover': 'backgroundColor: green[700]', marginRight: '10px'
                }}
              ><Save style={{ marginRight: 'theme.spacing.unit' }} />
                Save</Button>
              {loading && <CircularProgress size={24}
                style={{
                  color: 'green[500]', position: 'absolute', top: '50%',
                  left: '50%', marginTop: '-12', marginLeft: '-40'
                }} />}
              <Button raised color="default">
                <Cancel style={{ marginRight: 'theme.spacing.unit' }} />
                Clear
              </Button>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

UpdateUser.propTypes = {
  useredit: PropTypes.object,
  usereditActions: PropTypes.func.isRequired,
  ActionUpdateUser: React.PropTypes.func.isRequired
}

UpdateUser.contextTypes = {
  router: React.PropTypes.object.isRequired
}

function MapStateToProps(state) {
  return {
    useredit: state.useredit
  }
}

const MapDispatchToProps = dispatch => {
  return {
    usereditActions: bindActionCreators(usereditActions, dispatch),
    ActionUpdateUser: bindActionCreators(ActionUpdateUser, dispatch)
  }
}

export default withRouter(connect(MapStateToProps, MapDispatchToProps)(UpdateUser));