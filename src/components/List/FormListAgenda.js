import React, { Component, PropTypes } from 'react';
import { withRouter } from 'react-router-dom';
import { Table, Button, Pagination } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import * as agendaActions from 'redux-modules/modules/agenda';
import * as agendaEditActions from 'redux-modules/modules/agendaedit';
import ButtonMaterial from 'material-ui/Button';
import Delete from 'material-ui-icons/Delete';
import Edit from 'material-ui-icons/Edit';
import Dialog, {
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
	withMobileDialog,
} from 'material-ui/Dialog';
import request from 'superagent';
import Exclamation from './../../image/exclamation-mark.png';
import { WordLimiter } from './../../common/LimitKata';

class FormListAgenda extends Component {
	constructor(props) {
		super(props);
		this.state = {
			agenda: { response: { data: [] } },
			errors: false,
			loading: false,
			Page: 1,
			activePage: 1,
			selectedAgendaId: null,
			open: false
		}
		this.handleSelect  = this.handleSelect.bind(this);
		this.handleDelete  = this.handleDelete.bind(this);
		this.handleClose   = this.handleClose.bind(this);
		this.handleConfirm = this.handleConfirm.bind(this);
		this.addAgenda     = this.addAgenda.bind(this);
	}

	addAgenda() {
		window.location.href = `${process.env.REACT_APP_HOST}/addagenda`;
	}

	handleDelete(selectedAgendaId) {
		this.setState({ open: true, selectedAgendaId });
	}

	handleClose() {
		this.setState({ open: false, selectedAgendaId: null });
	}

	handleConfirm() {
		const data = {
			id_agenda: this.state.selectedAgendaId
		}
		request.post(`${process.env.REACT_APP_APIHOST}/v1/deleteagenda`)
			.send(data)
			.then(success => {
				this.props.agendaActions.DeleteAgenda(this.state.selectedAgendaId);
				this.setState({ open: false, selectedAgendaId: null });
			}, error => {
				this.setState({ open: false, selectedAgendaId: null });
			})
		//console.log("masuk");
	}

	handleSelect(page) {
		this.props.agendaActions.ListAgenda(page);
	}
	componentWillMount() {
		this.props.agendaActions.ListAgenda(1);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.agenda && nextProps.agenda.response) {
			console.log(nextProps.agenda);
			nextProps.agenda.response.data = nextProps.agenda.response.data.map(agenda => {
				return {
					id_agenda: agenda.id_agenda,
					title: agenda.title,
					start_date: new Date(agenda.start_date).toLocaleString(),
					end_date: new Date(agenda.end_date).toLocaleString(),
					desc: agenda.desc
				}
			});
			console.log('nextAgenda', nextProps.agenda.response.data);
			this.setState({ agenda: nextProps.agenda })
		}
	}

	render() {
		const { fullScreen } = this.props;
		return (
			<div className="container">
				<div>
					<h2>List Agenda</h2>
					<button type="button" className="btn btn-primary" style={{ float: 'right', marginBottom: '10px' }}
						onClick={this.addAgenda}>Tambah Agenda</button>
				</div>
				<Table striped bordered condensed hover>
					<thead>
						<tr>
							<th style={{ textAlign: "center" }}>No</th>
							<th style={{ textAlign: "center" }}>Judul agenda</th>
							<th style={{ textAlign: "center" }}>Start date</th>
							<th style={{ textAlign: "center" }}>End date</th>
							<th style={{ textAlign: "center" }}>Description</th>
							<th style={{ textAlign: "center" }}>Edit</th>
							<th style={{ textAlign: "center" }}>Delete</th>
						</tr>
					</thead>
					<tbody>
						{this.state.agenda.response && this.state.agenda.response.data && this.state.agenda.response.data.length === 0 &&
							<tr><td colSpan="7" style={{ textAlign: 'center' }}>Tidak ada Data</td></tr>
						}
						{this.state.agenda.response && this.state.agenda.response.data && this.state.agenda.response.data.length > 0 && this.state.agenda.response.data.map((agenda, key) =>
							<tr key={agenda.id_agenda}>
								<td>{((this.state.agenda.response.page - 1) * 10) + parseInt(key) + 1}</td>
								<td>{WordLimiter(agenda.title, 15)}</td>
								<td>{agenda.start_date}</td>
								<td>{agenda.end_date}</td>
								<td>{WordLimiter(agenda.desc, 20)}</td>
								<td onClick={event => {
									this.props.agendaEditActions.setEditAgenda(agenda);
									this.props.history.push(`/updateagenda/${agenda.id_agenda}`);
								}}><Edit /></td>
								<td><ButtonMaterial raised color="default" onClick={() => this.handleDelete(agenda.id_agenda)}>
									<Delete style={{ marginRight: 'theme.spacing.unit' }} />
								</ButtonMaterial>
								</td>
							</tr>
						)}
					</tbody>
					<Dialog
						fullScreen={fullScreen}
						open={this.state.open}
						onClose={this.handleClose}
						aria-labelledby="confirm delete">
						{/* <DialogTitle id="confirm delete">{"Hapus Data"}</DialogTitle> */}
						<DialogContent>
							<img src={Exclamation} width={200} height={200} />
							<br />
							<p style={{ fontWeight: "bold" }}>
								Anda yakin mau hapus data?
              </p>
						</DialogContent>
						<div style={{ marginBottom: "10px", marginTop: "-20px" }}>
							<button onClick={this.handleConfirm}
								style={{
									marginRight: "3px", padding: "8px",
									backgroundColor: "#1a75ff", borderRadius: "5px",
									color: "white", border: "none"
								}}>Ya, Hapus ini</button>
							<button onClick={this.handleClose}
								style={{
									padding: "8px", color: "white",
									backgroundColor: "#ff1a1a", borderRadius: "5px", border: "none"
								}}>Tidak</button>
						</div>
					</Dialog>
				</Table>
				<div>
					<div className="Paging" style={{ margin: 'auto', width: '17%' }}>
						<Pagination
							prev
							next
							first
							last
							ellipsis
							boundaryLinks
							items={this.state.agenda.response.total % 10 < 1 ? this.state.agenda.response.total / 10 : (this.state.agenda.response.total / 10) + 1}
							maxButtons={5}
							activePage={this.state.agenda.response.page}
							onSelect={this.handleSelect} />
					</div>
				</div>
			</div>
		);
	}
}

FormListAgenda.propTypes = {
	agenda: PropTypes.object,
	agendaActions: PropTypes.func.isRequired
}

FormListAgenda.contextTypes = {
	router: React.PropTypes.object.isRequired
}

function MapStateToProps(state) {
	return {
		agenda: state.agenda
	}
}

const MapDispatchToProps = dispatch => ({
	agendaActions: bindActionCreators(agendaActions, dispatch),
	agendaEditActions: bindActionCreators(agendaEditActions, dispatch)
})

export default withRouter(connect(MapStateToProps, MapDispatchToProps)(FormListAgenda));