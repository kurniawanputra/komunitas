import React, { Component } from 'react';
import Instagram from './../../image/instagram.png';
import Facebook  from './../../image/facebook.png';
import Twitter   from './../../image/twitter.png';

export default class Footer extends Component {
  render() {
    return (
      <div>
        <hr />
          <div className="sosmed margtop">
            <ul className="list-inline">
              <li><a href="#"><img className="thumbnail" src={Instagram} width={50} height={50} style={{ border: "none", boxShadow: "none"}} /></a></li>
              <li><a href="#"><img className="thumbnail" src={Facebook} width={50} height={50} style={{ border: "none", boxShadow: "none"}} /></a></li>
              <li><a href="#"><img className="thumbnail" src={Twitter} width={50} height={50} style={{ border: "none", boxShadow: "none"}} /></a></li>
            </ul>
          </div>
        <hr />
          <p>copyright@komunitas{new Date().getFullYear()}</p>
      </div>
    );
  }
}