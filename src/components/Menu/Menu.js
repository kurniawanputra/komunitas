import React, { Component, PropTypes} from "react";
import {Navbar, Nav, NavItem, NavDropdown, MenuItem} from 'react-bootstrap';
import { Logout } from './../../common/authLogin';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';


class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nama : ''
    }

    this.logout     = this.logout.bind(this);
    this.home       = this.home.bind(this);
    this.profil     = this.profil.bind(this);
    this.jadwal     = this.jadwal.bind(this);
    this.berita     = this.berita.bind(this);
    this.gallery    = this.gallery.bind(this);
    this.hubungi    = this.hubungi.bind(this);
    this.login      = this.login.bind(this);
    this.listUser   = this.listUser.bind(this);
    this.listBerita = this.listBerita.bind(this);
    this.listAgenda = this.listAgenda.bind(this);
  }

  home(event) {
    //window.location.href = `${process.env.REACT_APP_HOST}/home`;
    event.preventDefault();
    this.props.history.push('/home');
  }

  profil(event) {
    //window.location.href = `${process.env.REACT_APP_HOST}/profil`;
    event.preventDefault();
    this.props.history.push('/profil');
  }

  jadwal(event) {
    // window.location.href = `${process.env.REACT_APP_HOST}/agenda`;
    event.preventDefault();
    this.props.history.push('/agenda');
  }

  berita(event) {
    // window.location.href = `${process.env.REACT_APP_HOST}/berita`;
    event.preventDefault();
    this.props.history.push('/berita');
  }

  gallery(event) {
    event.preventDefault();
    this.props.history.push('/foto');
    //window.location.href = `${process.env.REACT_APP_HOST}/foto`;
  }

  hubungi(event) {
    //window.location.href = `${process.env.REACT_APP_HOST}/hubungi`;
    event.preventDefault();
    this.props.history.push('/hubungi');
  }

  login(event) {
    event.preventDefault();
    this.props.history.push('/login');
    //window.location.href = `${process.env.REACT_APP_HOST}/login`;
  }

  listUser(event) {
    event.preventDefault();
    this.props.history.push('/listuser');
    //window.location.href = `${process.env.REACT_APP_HOST}/listuser`;
  }

  listBerita(event) {
    event.preventDefault();
    this.props.history.push('/listberita');
    //window.location.href = `${process.env.REACT_APP_HOST}/listberita`;
  }

  listAgenda(event) {
    event.preventDefault();
    this.props.history.push('/listagenda');
    //window.location.href = `${process.env.REACT_APP_HOST}/listagenda`;
  }

  logout(e) {
    e.preventDefault();
    this.props.Logout();
  }

  render() {
    const user = localStorage.getItem('token');
    const admin =(
      <Navbar inverse collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="#" onClick={this.home}>Komunitas Pijar</a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              <NavItem eventKey={1} href="#" onClick={this.home}>Beranda</NavItem>
              <NavItem eventKey={2} href="#" onClick={this.profil}>Profil</NavItem>
              <NavItem eventKey={3} href="#" onClick={this.jadwal}>Agenda</NavItem>
              <NavItem eventKey={4} href="#" onClick={this.berita}>Berita</NavItem>
              <NavItem eventKey={5} href="#" onClick={this.gallery}>Gallery</NavItem>
              <NavItem eventKey={6} href="#" onClick={this.hubungi}>Hubungi Kami</NavItem>
              <NavItem eventKey={7} href="#" onClick={this.listUser}>List User</NavItem>
              <NavItem eventKey={8} href="#" onClick={this.listBerita}>List Berita</NavItem>
              <NavItem eventKey={9} href="#" onClick={this.listAgenda}>List Agenda</NavItem>
            </Nav>
            <Nav pullRight>
              <NavItem eventKey={1} href="#" onClick={this.logout}>Logout</NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
    );

    const tamu =(
      <Navbar inverse collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="#" onClick={this.home}>Komunitas</a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              <NavItem eventKey={1} href="#" onClick={this.home}>Beranda</NavItem>
              <NavItem eventKey={2} href="#" onClick={this.profil}>Profil</NavItem>
              <NavItem eventKey={3} href="#" onClick={this.jadwal}>Agenda</NavItem>
              <NavItem eventKey={4} href="#" onClick={this.berita}>Berita</NavItem>
              <NavItem eventKey={5} href="#" onClick={this.gallery}>Gallery</NavItem>
              <NavItem eventKey={6} href="#" onClick={this.hubungi}>Hubungi Kami</NavItem>
            </Nav>
            <Nav pullRight>
              <NavItem eventKey={1} href="#" onClick={this.login}>Login</NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
    );
    return (
      <div>
        { user ? admin : tamu }
      </div>
    );
  }
}

Menu.propTypes = {
  auth: React.PropTypes.object.isRequired,
  Logout: PropTypes.func.isRequired
}


function MapStateToProps(state) {
  return {
    auth: state.auth
  }
}


export default withRouter(connect(MapStateToProps, {Logout})(Menu));