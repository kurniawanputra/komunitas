import React from 'react';

export default class ImagePreview extends React.Component {
  state = {
    src: ''
  }
  componentDidMount() {
    this.draw(this.props.file);
  }
  componentWillReceiveProps(nextProps) {
    this.draw(nextProps.file);
  }
  draw = file => {
    const reader = new FileReader();
    reader.onloadend = () => {
      this.setState({ src: reader.result });
    }
    reader.readAsDataURL(file);
  }
  render() {
    const { file, ...rest } = this.props;
    const { src } = this.state;
    return (
      <img src={src} {...rest} />
    );
  }
}