import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { AddBerita } from './../../common/beritaAction';
import { CircularProgress } from 'material-ui/Progress';
import { Alert } from 'react-bootstrap';
import Button from 'material-ui/Button';
import green from 'material-ui/colors/green';
import Save from 'material-ui-icons/Save';
import Cancel from 'material-ui-icons/Cancel';


class FormAddBerita extends Component {
  constructor(props) {
    super(props);
    this.state = {
      judul_berita: '',
      deskripsi_berita: '',
      isi_berita: '',
      kategori_berita: '',
      imagePreviewURL: '',
      loading: false,
      success: false,
      alertError: false
    }
    this.handelChange = this.handelChange.bind(this);
    this.handlePhoto = this.handlePhoto.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleError = this.handleError.bind(this);
  }

  handleError() {
    this.setState({ alertError : false});
  }

  handelChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }
  handlePhoto(e) {
    e.preventDefault();
    const reader = new FileReader();
    var file = e.target.files[0];
    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewURL: reader.result
      });
    }
    reader.readAsDataURL(file);
    console.log(file);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  timer = undefined;

  handleSubmit(e) {
    e.preventDefault();
    const judul_berita = this.state.judul_berita;
    const deskripsi_berita = this.state.deskripsi_berita;
    const isi_berita = this.state.isi_berita;
    const kategori_berita = this.state.kategori_berita;
    const file = this.state.file;
    if (judul_berita === "" || deskripsi_berita === "" || isi_berita === "" || kategori_berita === "0" || file.size > 2097152) {
      //alert("harapa lengkap isi data");
      this.timer = setTimeout(() => this.setState({ loading: false }), 2000);
      this.setState({ alertError: true});
    }
    else {
      this.setState({ loading: true });
      const data = new FormData();
      data.append("judul_berita", this.state.judul_berita);
      data.append("deskripsi_berita", this.state.deskripsi_berita);
      data.append("isi_berita", this.state.isi_berita);
      data.append("id_kategori_berita", this.state.kategori_berita);
      data.append("file", this.state.file);
      //console.log(data);
      this.props.AddBerita(data, this.props.history.push);
    }
  }


  render() {
    let { imagePreviewURL, loading, success, alertError } = this.state;
    let $imagePreview = null;
    if (imagePreviewURL) {
      $imagePreview = (<img src={imagePreviewURL} width={230}
        height={200} style={{
          border: '1px solid #ddd', borderRadius: '4px',
          webkitBorderRadius: '24px', padding: '5px', float: 'left'
        }} />)
    }
    return (
      <div className="container">
        {alertError && <Alert bsStyle="danger" onDismiss={this.handleError}>
          <h4>Mohon lengkapi Isi Data</h4>
          </Alert>
        }
        <h2>Form Tambah Berita</h2>
        <form className="form-horizontal" enctype="multipart/form-data">
          <div className="form-group">
            <label className="control-label col-sm-2">Judul Berita</label>
            <div className="col-sm-10">
              <input type="text" name="judul_berita" value={this.state.judul_berita} onChange={this.handelChange} className="form-control" placeholder="Masukkan judul berita"></input>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Deskripsi Berita</label>
            <div className="col-sm-10">
              <input type="text" name="deskripsi_berita" value={this.state.deskripsi_berita} onChange={this.handelChange} className="form-control" placeholder="Masukkan deskripsi berita"></input>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Isi Berita</label>
            <div className="col-sm-10">
              <textarea className="form-control" name="isi_berita" value={this.state.isi_berita} onChange={this.handelChange} rows="5" id="comment" placeholder="silahkan masukkan isi berita"></textarea>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Kategori Berita</label>
            <div className="col-sm-10">
              <select className="form-control" name="kategori_berita" value={this.state.kategori_berita} onChange={this.handelChange} placeholder="pilih kategori berita">
                <option value="0">Silahkan Pilih Kategori Berita</option>
                <option value="1">Agama</option>
                <option value="2">Humaniora</option>
                <option value="3">Pendidikan</option>
                <option value="4">Sosial</option>
              </select>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Photo Berita</label>
            <div className="col-sm-10">
              <input type="file" ref="file" name="file" className="form-control" onChange={this.handlePhoto} />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2"></label>
            <div className="col-sm-10">
              {$imagePreview}
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <Button raised color="primary" disabled={loading} onClick={this.handleSubmit}
                style={{backgroundColor : '#86ec65', 
                '&:hover': 'backgroundColor: green[700]', marginRight: '10px'}}
                ><Save style={{marginRight : 'theme.spacing.unit'}} />
                Save</Button>
                {loading && <CircularProgress size={24} 
                style={{color: 'green[500]', position: 'absolute', top: '50%',
                left: '50%', marginTop: '-12', marginLeft: '-40'}} />}
              <Button raised color="default">
              <Cancel style={{marginRight : 'theme.spacing.unit'}} />
              Clear
              </Button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

FormAddBerita.propTypes = {
  AddBerita: React.PropTypes.func.isRequired
}

FormAddBerita.contextTypes = {
  router: React.PropTypes.object.isRequired
}

export default withRouter(connect(null, { AddBerita })(FormAddBerita));