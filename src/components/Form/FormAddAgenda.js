import React, { Component, propTypes, func } from 'react';
import DateTime from 'react-datetime';
import moment, { locale } from 'moment';
import { connect } from 'react-redux';
import { AddAgenda } from './../../common/agendaAction';
import { Alert } from 'react-bootstrap';
//require('moment/locale/id');

class FormAddAgenda extends Component {
  constructor(props) {
    super(props);
    this.state = {
      start: moment(),
      judul: '',
      end: moment(),
      desc: '',
      alertError: false
    };

    this.handleStartDate = this.handleStartDate.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleEndDate = this.handleEndDate.bind(this);
    this.handleError = this.handleError.bind(this);
  }

  handleError() {
    this.setState({ alertError: false});
  }

  handleStartDate(e) {
    const sd = moment(e).format("YYYY-MM-DD HH:m:s")
    this.setState({ start: sd });
  }

  handleEndDate(e) {
    const ed = moment(e).format("YYYY-MM-DD HH:m:s")
    this.setState({ end: ed });
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.judul === "" || this.state.desc === "") {
      this.setState({ alertError : true });
    } else {
      const data = {
        title: this.state.judul,
        start_date: this.state.start,
        end_date: this.state.end,
        desc: this.state.desc
      }
      console.log(data);
      this.props.AddAgenda(data, this.context.router.push);
    }
  }

  render() {
    let {alertError} = this.state;
    return (
      <div className="container">
        {alertError && <Alert bsStyle="danger" onDismiss={this.handleError}>
          <h4>mohon lengkapi data</h4>
          </Alert>
        }
        <h2>Form Tambah Agenda</h2>
        <form onSubmit={this.handleSubmit} className="form-horizontal">
          <div className="form-group">
            <label className="control-label col-sm-2">Judul : </label>
            <div className="col-sm-10">
              <input type="text" name="judul" className="form-control" placeholder="Judul agenda" value={this.state.judul} onChange={this.handleChange} />
            </div>
          </div>

          <div className="form-group">
            <label className="control-label col-sm-2">masukkan start date</label>
            <div className="col-sm-10">
              <DateTime open={false} input={true} onChange={this.handleStartDate} dateFormat="YYYY-MM-DD" timeFormat={true} />
            </div>
          </div>

          <div className="form-group">
            <label className="control-label col-sm-2">masukkan end date</label>
            <div className="col-sm-10">
              <DateTime open={false} input={true} onChange={this.handleEndDate} dateFormat="YYYY-MM-DD" timeFormat={true} />
            </div>
          </div>

          <div className="form-group">
            <label className="control-label col-sm-2">Deskripsi : </label>
            <div className="col-sm-10">
              <input type="text" name="desc" className="form-control" placeholder="deskripsi" value={this.state.desc} onChange={this.handleChange} />
            </div>
          </div>

          <div className="form-group">
            <label className="control-label col-sm-2"></label>
            <div className="col-sm-10">
              <input type="submit" value={"Submit"} />
            </div>
          </div>
        </form>
      </div>
    )
  }
}

FormAddAgenda.propTypes = {
  AddAgenda: React.PropTypes.func.isRequired
}

FormAddAgenda.contextTypes = {
  router: React.PropTypes.object.isRequired
}

export default connect(null, { AddAgenda })(FormAddAgenda);

