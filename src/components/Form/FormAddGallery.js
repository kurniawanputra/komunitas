import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ImagePreview from './ImagePreview';
import { connect } from 'react-redux';
import { AddGallery } from './../../common/galleryAction';
import { Alert } from 'react-bootstrap';

let imagePreviewURLs = [];

class FormAddGallery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      files: [],
      imagePreviewURLs: [],
      pictures: [],
      alertError: false
    }

    this.handleImage = this.handleImage.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleError = this.handleError.bind(this);
  }

  handleError() {
    this.setState({ alertError: false });
  }

  handleImage(e) {
    e.preventDefault();
    var files = [];
    for (var i = 0; i < e.target.files.length; i++) {
      if (e.target.files[i].size < 595284) {
        files.push(e.target.files[i]);
      } else {
        alert('file size exceeded');
      }
    }
    if (files.length > 5) {
      files = files.slice(0, 5);
    }
    this.setState({ files }, () => console.log(files));
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.file === '') {
      this.setState({ alertError: true });
    } else {
      const data = new FormData();
      //data.append('judul_gallery', 'apaaja');
      for (let i = 0; i < this.state.files.length; i++) {
        data.append('data[]', this.state.files[i]);
      }
      this.props.AddGallery(data, this.props.history.push);
    }

  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  render() {
    let { files, alertError } = this.state;
    return (
      <div>
        <div className="container">
          {alertError && <Alert bsStyle="danger" onDismiss={this.handleError}>
            <h4>Mohon masukkan file gambar</h4>
            </Alert>
          }
          <form onSubmit={this.handleSubmit} enctype="multipart/form-data">
            <div className="form-inline">
              <label>Masukkan judul</label>
              <input type="text" name="judul" className="form-control" placeholder="masukkan judul" onChange={this.handleChange} />
            </div>

            <div className="form-inline">
              <label>Masukkan gambar</label>
              <input id="file" type="file" multiple name="file" ref="file" className="form-control" onChange={this.handleImage} placeholder="masukkan gambar" />
            </div>
            <div className="form-inline">
              {files.map((file, key) => {
                return (<ImagePreview file={file}
                  key={key} alt={file.name} width="120" height="120"
                  style={{ marginLeft: '5px', border: '4px solid #c7c7c7', borderRadius: '24px', webkitBorderRadius: '24px' }} />);
              }
              )}
            </div>
            <div className="form-inline">
              <label></label>
              <input type="submit" value="submit" />
            </div>
          </form>
        </div>
      </div>
    )
  }
}

FormAddGallery.propTypes = {
  AddGallery: React.PropTypes.func.isRequired
}

FormAddGallery.contextTypes = {
  router: React.PropTypes.object.isRequired
}

export default withRouter(connect(null, { AddGallery })(FormAddGallery));
