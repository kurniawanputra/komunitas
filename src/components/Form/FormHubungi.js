import React, { Component } from 'react';
import { Grid, Col, FormGroup, label, Button } from 'react-bootstrap';


export default class FormHubungi extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nama: '',
      email: '',
      subjek: '',
      isi: ''
    }

    this.handleChange = this.handleChange.bind(this);

  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }


  render() {

    return (
      <div className="container">
        <h2>Hubungi Kami</h2>
        <form className="form-horizontal" enctype="multipart/form-data">
          <div className="form-group">
            <label className="control-label col-sm-2" for="lblNama">Nama Lengkap :   </label>
            <div className="col-sm-10">
              <input type="text" name="nama" value={this.state.nama} onChange={this.handleChange} placeholder="Nama Lengkap" className="form-control" />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2" for="lblEmail">Email :   </label>
            <div className="col-sm-10">
              <input type="text" name="nama" value={this.state.email} onChange={this.handleChange} placeholder="Email" className="form-control" />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2" for="lblsubject">Subjek :   </label>
            <div className="col-sm-10">
              <input type="text" name="nama" value={this.state.subjek} onChange={this.handleChange} placeholder="Subjek" className="form-control" />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2" for="lblPesan">Pesan anda :   </label>
            <div className="col-sm-10">
              <textarea rows="4" name="isi" value={this.state.isi} onChange={this.handleChange} cols="70" placeholder="silahkan isi pesan anda" className="form-control">
              </textarea>
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <Button type="submit">Submit</Button>
              <Button type="submit">Clear</Button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}


