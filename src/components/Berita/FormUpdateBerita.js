import React, { Component, PropTypes } from 'react';
import * as beritaEditActions from 'redux-modules/modules/beritaedit';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { CircularProgress } from 'material-ui/Progress';
import Button from 'material-ui/Button';
import green from 'material-ui/colors/green';
import Save from 'material-ui-icons/Save';
import Cancel from 'material-ui-icons/Cancel';
import { Alert } from 'react-bootstrap';
import { UpdateBerita } from './../../common/beritaAction';

class FormUpdateBerita extends Component {
  constructor(props) {
    super(props);
    console.log('props', props);
    let judul_berita = '',
      deskripsi_berita = '',
      isi_berita = '',
      kategori_berita = '',
      image_berita = '',
      id_berita = '';
    if (props.beritaedit && props.beritaedit.loaded && props.beritaedit.response) {
      judul_berita = props.beritaedit.response.judul_berita;
      deskripsi_berita = props.beritaedit.response.deskripsi_berita;
      isi_berita = props.beritaedit.response.isi_berita;
      kategori_berita = props.beritaedit.response.kategori_berita;
      image_berita = props.beritaedit.response.image_berita;
      id_berita = props.beritaedit.response.id_berita;
    }
    this.state = {
      judul_berita,
      deskripsi_berita,
      isi_berita,
      kategori_berita,
      image_berita,
      id_berita,
      imagePreviewURL: '',
      loading: false,
      success: false,
      alertError: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.handlePhoto = this.handlePhoto.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleError = this.handleError.bind(this);
  }

  handleError() {
    this.setState({ alertError: false });
  }

  handlePhoto(e) {
    e.preventDefault();
    const reader = new FileReader();
    var file = e.target.files[0];
    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewURL: reader.result
      });
    }
    reader.readAsDataURL(file);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.judul_berita === "" || this.state.deskripsi_berita === "" || this.state.id_berita ==="" || this.state.kategori_berita === "0") {
      this.timer = setTimeout(() => this.setState({ loading: false }), 2000);
      this.setState({ alertError: true });
    } else {
      this.setState({ loading: true });
      const data = new FormData();
      data.append("id_berita", this.state.id_berita);
      data.append("judul_berita", this.state.judul_berita);
      data.append("deskripsi_berita", this.state.deskripsi_berita);
      data.append("isi_berita", this.state.isi_berita);
      data.append("kategori_berita", this.state.kategori_berita);
      data.append("file", this.state.file);
      //console.log(data);
      this.props.UpdateBerita(data, this.props.history.push);
    }
    //this.props.ActionUpdateBerita(data, this.props.history.push);
  }

  timer = undefined;

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  componentWillMount() {
    if (!this.props.beritaedit.loaded) {
      this.props.beritaEditActions.loadEditBerita(this.props.match.params.id);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.beritaedit && nextProps.beritaedit.loaded && nextProps.beritaedit.response) {
      console.log(nextProps.beritaedit);
      console.log('berita edit', nextProps.beritaedit.response.data);
      this.setState({ beritaedit: nextProps.beritaedit })
    }
  }


  render() {
    let { imagePreviewURL, loading, success, alertError } = this.state;
    let $imagePreview = null;
    if (imagePreviewURL) {
      $imagePreview = (<img src={imagePreviewURL} width={230}
        height={200} style={{
          border: '1px solid #ddd', borderRadius: '4px',
          webkitBorderRadius: '24px', padding: '5px', float: 'left'
        }} />)
    }
    return (
      <div className="container">
        {alertError && <Alert bsStyle="danger" onDismiss={this.handleError}>
          <h4>Mohon lengkapi Form</h4>
        </Alert>
        }
        <h1>Form update berita</h1>
        <form className="form-horizontal" enctype="multipart/form-data">
          <div className="form-group">
            <label className="control-label col-sm-2">Id Berita</label>
            <div className="col-sm-10">
            <input type="text" disabled name="id_berita" className="form-control" value={this.state.id_berita}></input>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Judul Berita</label>
            <div className="col-sm-10">
              <input type="text" name="judul_berita" value={this.state.judul_berita} onChange={this.handleChange} className="form-control" placeholder="Masukkan judul berita"></input>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Deskripsi Berita</label>
            <div className="col-sm-10">
              <input type="text" name="deskripsi_berita" className="form-control" value={this.state.deskripsi_berita} onChange={this.handleChange} placeholder="Masukkan judul berita"></input>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Isi Berita</label>
            <div className="col-sm-10">
              <textarea className="form-control" name="isi_berita" value={this.state.isi_berita} onChange={this.handleChange} rows="5" id="comment" placeholder="silahkan masukkan isi berita"></textarea>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Kategori Berita</label>
            <div className="col-sm-10">
              <select className="form-control" name="kategori_berita" value={this.state.kategori_berita} onChange={this.handleChange} placeholder="pilih kategori berita">
                <option value="0">Silahkan Pilih Kategori Berita</option>
                <option value="1">Agama</option>
                <option value="2">Humaniora</option>
                <option value="3">Pendidikan</option>
                <option value="4">Sosial</option>
              </select>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Gambar lama</label>
            <div className="col-sm-10">
              <img src={`${process.env.REACT_APP_APIHOST}/singlefoto/${this.state.image_berita}`}
                width={100} height={100} style={{ float: 'left' }}></img>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Gambar Berita Baru</label>
            <div className="col-sm-10">
              <input type="file" ref="file" name="file" className="form-control" onChange={this.handlePhoto}></input>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2"></label>
            <div className="col-sm-10">
              {$imagePreview}
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2"></label>
            <div className="col-sm-10">
              <Button raised color="primary" disabled={loading} onClick={this.handleSubmit}
                style={{
                  backgroundColor: '#86ec65',
                  '&:hover': 'backgroundColor: green[700]', marginRight: '10px'
                }}
              ><Save style={{ marginRight: 'theme.spacing.unit' }} />
                Save</Button>
              {loading && <CircularProgress size={24}
                style={{
                  color: 'green[500]', position: 'absolute', top: '50%',
                  left: '50%', marginTop: '-12', marginLeft: '-40'
                }} />}
              <Button raised color="default">
                <Cancel style={{ marginRight: 'theme.spacing.unit' }} />
                Clear
              </Button>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

FormUpdateBerita.propTypes = {
  beritaedit: PropTypes.object,
  beritaEditActions: PropTypes.func.isRequired,
  UpdateBerita: React.PropTypes.func.isRequired
}

FormUpdateBerita.contextTypes = {
  router: React.PropTypes.object.isRequired
}


function MapStateToProps(state) {
  return {
    beritaedit: state.beritaedit
  }
}

const MapDispatchToProps = dispatch => {
  return {
    beritaEditActions: bindActionCreators(beritaEditActions, dispatch),
    UpdateBerita: bindActionCreators(UpdateBerita, dispatch)
  }
}

export default withRouter(connect(MapStateToProps, MapDispatchToProps)(FormUpdateBerita));
