import React, { Component, PropTypes } from 'react';
import BeritaJson from './../../JSON/berita';
import Card, { CardHeader, CardActions, CardContent, CardMedia } from 'material-ui/Card';
import { Pagination } from 'react-bootstrap';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import { withRouter } from 'react-router-dom';
import red from 'material-ui/colors/red';
import Avatar from 'material-ui/Avatar';
import { withStyles } from 'material-ui/styles';
import Tabs, { Tab } from 'material-ui/Tabs';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as AgamaActions from 'redux-modules/modules/beritaagama';
import * as HumanioraActions from 'redux-modules/modules/beritahumaniora';
import * as PendidikanActions from 'redux-modules/modules/beritapendidikan';
import * as SosialActions from 'redux-modules/modules/beritasosial';
import * as beritaDetailActions from 'redux-modules/modules/beritadetail';
import { WordLimiter } from './../../common/LimitKata';


function TabContainer(props) {
  return <div style={{ padding: 8 * 3 }}>{props.children}</div>;
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

class ListBerita extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      beritaagama: { response: { data: [] } },
      beritahumaniora: { response: { data: [] } },
      beritapendidikan: { response: { data: [] } },
      beritasosial: { response: { data: [] } },
      page: 1,
      activePage: 1
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSelectAgama = this.handleSelectAgama.bind(this);
    this.handleSelectHumaniora = this.handleSelectHumaniora.bind(this);
    this.handleSelectPendidikan = this.handleSelectPendidikan.bind(this);
    this.handleSelectSosial = this.handleSelectSosial.bind(this);
  }

  handleSelectAgama(page) {
    this.props.AgamaActions.ListAgama(page);
  }

  handleSelectHumaniora(page) {
    this.props.HumanioraActions.ListHumaniora(page);
  }

  handleSelectPendidikan(page) {
    this.props.PendidikanActions.ListPendidikan(page);
  }

  handleSelectSosial(page) {
    this.props.SosialActions.ListSosial(page);
  }

  componentWillMount() {
    this.props.AgamaActions.ListAgama(1);
    this.props.HumanioraActions.ListHumaniora(1);
    this.props.PendidikanActions.ListPendidikan(1);
    this.props.SosialActions.ListSosial(1);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.beritaagama && nextProps.beritaagama.response) {
      //console.log(nextProps.beritaagama);
      nextProps.beritaagama.response.data = nextProps.beritaagama.response.data.map(beritaagama => {
        return {
          id_berita: beritaagama.id_berita,
          deskripsi_berita: beritaagama.deskripsi_berita,
          isi_berita: beritaagama.isi_berita,
          judul_berita: beritaagama.judul_berita,
          nama_kategori: beritaagama.nama_kategori,
          timestamp: new Date(beritaagama.timestamp).toLocaleString(),
          image_berita: beritaagama.image_berita
        }
      });
      this.setState({ beritaagama: nextProps.beritaagama })
    }
    if (nextProps.beritahumaniora && nextProps.beritahumaniora.response) {
      nextProps.beritahumaniora.response.data = nextProps.beritahumaniora.response.data.map(beritahumaniora => {
        return {
          id_berita: beritahumaniora.id_berita,
          deskripsi_berita: beritahumaniora.deskripsi_berita,
          isi_berita: beritahumaniora.isi_berita,
          judul_berita: beritahumaniora.judul_berita,
          nama_kategori: beritahumaniora.nama_kategori,
          timestamp: new Date(beritahumaniora.timestamp).toLocaleString(),
          image_berita: beritahumaniora.image_berita
        }
      })
      //console.log(nextProps.beritahumaniora);
      this.setState({ beritahumaniora: nextProps.beritahumaniora })
    }
    if (nextProps.beritapendidikan && nextProps.beritapendidikan.response) {
      //console.log(nextProps.beritapendidikan);
      nextProps.beritapendidikan.response.data = nextProps.beritapendidikan.response.data.map(beritapendidikan => {
        return {
          id_berita: beritapendidikan.id_berita,
          deskripsi_berita: beritapendidikan.deskripsi_berita,
          isi_berita: beritapendidikan.isi_berita,
          judul_berita: beritapendidikan.judul_berita,
          nama_kategori: beritapendidikan.nama_kategori,
          timestamp: new Date(beritapendidikan.timestamp).toLocaleString(),
          image_berita: beritapendidikan.image_berita
        }
      })
      this.setState({ beritapendidikan: nextProps.beritapendidikan })
    }
    if (nextProps.beritasosial && nextProps.beritasosial.response) {
      //console.log(nextProps.beritasosial);
      nextProps.beritasosial.response.data = nextProps.beritasosial.response.data.map(beritasosial => {
        return {
          id_berita: beritasosial.id_berita,
          deskripsi_berita: beritasosial.deskripsi_berita,
          isi_berita: beritasosial.isi_berita,
          judul_berita: beritasosial.judul_berita,
          nama_kategori: beritasosial.nama_kategori,
          timestamp: new Date(beritasosial.timestamp).toLocaleString(),
          image_berita: beritasosial.image_berita
        }
      })
      this.setState({ beritasosial: nextProps.beritasosial })
    }
  }


  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleClick = (event, obj) => {
    this.props.beritaDetailActions.setDetailBerita(obj);
    this.props.history.push(`/beritadetail/${obj.id_berita}`);
  }

  render() {
    const { classes } = this.props;
    const { value } = this.state;
    return (
      <div>
        <Paper style={{ flexGrow: '1' }}>
          <h2>Berita</h2>
          <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            centered
          >
            <Tab label={<span style={{ color: 'black', fontSize: '15px' }}>Agama</span>} />
            <Tab label={<span style={{ color: 'black', fontSize: '15px' }}>Humaniora</span>} />
            <Tab label={<span style={{ color: 'black', fontSize: '15px' }}>Pendidikan</span>} />
            <Tab label={<span style={{ color: 'black', fontSize: '15px' }}>Sosial</span>} />
          </Tabs>
        </Paper>
        {value === 0 && <TabContainer>
          <div className="container">
            <Grid container>
              {this.state.beritaagama.response && this.state.beritaagama.response.data && this.state.beritaagama.response.data.length === 0 &&
                <h1>Tidak ada Data</h1>
              }
              {this.state.beritaagama.response && this.state.beritaagama.response.data && this.state.beritaagama.response.data.length > 0
                && this.state.beritaagama.response.data.map((beritaagama, key) =>
                  <Grid item xs={12} lg={3}>
                    <Card style={{ maxWidth: '345' }} key={beritaagama.id_berita}>
                      <CardHeader
                        title={WordLimiter(beritaagama.judul_berita, 30)}
                        subheader={beritaagama.timestamp}
                      />
                      <CardMedia
                        style={{ height: '200' }}
                        onClick={event => this.handleClick(event, beritaagama)}
                        //image={require(`..${beritaagama.image_berita}`)}
                        image={`${process.env.REACT_APP_APIHOST}/singlefoto/${beritaagama.image_berita}`}
                        title={beritaagama.deskripsi_berita}
                      />
                      <CardContent>
                        <p>
                          {WordLimiter(beritaagama.isi_berita, 80)}
                        </p>
                      </CardContent>
                    </Card>
                  </Grid>
                )}
            </Grid>
          </div>
          <div>
            <div className="Paging" style={{ margin: 'auto', width: '17%' }}>
              <Pagination
                prev
                next
                first
                last
                ellipsis
                boundaryLinks
                items={this.state.beritaagama.response.total % 10 < 1 ? this.state.beritaagama.response.total / 10 : (this.state.beritaagama.response.total / 10) + 1}
                maxButtons={5}
                activePage={this.state.beritaagama.response.page}
                onSelect={this.handleSelectAgama} />
            </div>
          </div>
        </TabContainer>}
        {value === 1 && <TabContainer>
          <div className="container">
            <Grid container>
              {this.state.beritahumaniora.response && this.state.beritahumaniora.response.data && this.state.beritahumaniora.response.data.length === 0 &&
                <h1>Tidak ada Data</h1>
              }
              {this.state.beritahumaniora.response && this.state.beritahumaniora.response.data && this.state.beritahumaniora.response.data.length > 0
                && this.state.beritahumaniora.response.data.map((beritahumaniora, key) =>
                  <Grid item xs={12} lg={3}>
                    <Card style={{ maxWidth: '345' }} key={beritahumaniora.id_berita}>
                      <CardHeader
                        title={WordLimiter(beritahumaniora.judul_berita, 20)}
                        subheader={beritahumaniora.timestamp}
                      />
                      <CardMedia
                        style={{ height: '200' }}
                        onClick={event => this.handleClick(event, beritahumaniora)}
                        //image={require(`./../..${beritaagama.image_berita}`)}
                        image={`${process.env.REACT_APP_APIHOST}/singlefoto/${beritahumaniora.image_berita}`}
                        title={beritahumaniora.deskripsi_berita}
                      />
                      <CardContent>
                        <p>
                          {WordLimiter(beritahumaniora.isi_berita, 80)}
                        </p>
                      </CardContent>
                    </Card>
                  </Grid>
                )}
            </Grid>
          </div>
          <div className="Paging" style={{ margin: 'auto', width: '17%' }}>
            <Pagination
              prev
              next
              first
              last
              ellipsis
              boundaryLinks
              items={this.state.beritahumaniora.response.total % 10 < 1 ? this.state.beritahumaniora.response.total / 10 : (this.state.beritahumaniora.response.total / 10) + 1}
              maxButtons={5}
              activePage={this.state.beritahumaniora.response.page}
              onSelect={this.handleSelectHumaniora} />
          </div>
        </TabContainer>}
        {value === 2 && <TabContainer>
          <div className="container">
            <Grid container>
              {this.state.beritapendidikan.response && this.state.beritapendidikan.response.data && this.state.beritapendidikan.response.data.length === 0 &&
                <h1>Tidak ada Data</h1>
              }
              {this.state.beritapendidikan.response && this.state.beritapendidikan.response.data && this.state.beritapendidikan.response.data.length > 0
                && this.state.beritapendidikan.response.data.map((beritapendidikan, key) =>
                  <Grid item xs={12} lg={3}>
                    <Card style={{ maxWidth: '345' }} key={beritapendidikan.id_berita}>
                      <CardHeader
                        title={WordLimiter(beritapendidikan.judul_berita, 20)}
                        subheader={beritapendidikan.timestamp}
                      />
                      <CardMedia
                        style={{ height: '200' }}
                        onClick={event => this.handleClick(event, beritapendidikan)}
                        //image={require(`./../..${beritaagama.image_berita}`)}
                        image={`${process.env.REACT_APP_APIHOST}/singlefoto/${beritapendidikan.image_berita}`}
                        title={beritapendidikan.deskripsi_berita}
                      />
                      <CardContent>
                        <p>
                          {WordLimiter(beritapendidikan.isi_berita, 80)}
                        </p>
                      </CardContent>
                    </Card>
                  </Grid>
                )}
            </Grid>
          </div>
          <div className="Paging" style={{ margin: 'auto', width: '17%' }}>
            <Pagination
              prev
              next
              first
              last
              ellipsis
              boundaryLinks
              items={this.state.beritapendidikan.response.total % 10 < 1 ? this.state.beritapendidikan.response.total / 10 : (this.state.beritapendidikan.response.total / 10) + 1}
              maxButtons={5}
              activePage={this.state.beritapendidikan.response.page}
              onSelect={this.handleSelectPendidikan} />
          </div>
        </TabContainer>}
        {value === 3 && <TabContainer>
          <div className="container">
            <Grid container>
              {this.state.beritasosial.response && this.state.beritasosial.response.data && this.state.beritasosial.response.data.length === 0 &&
                <h1>Tidak ada Data</h1>
              }
              {this.state.beritasosial.response && this.state.beritasosial.response.data && this.state.beritasosial.response.data.length > 0
                && this.state.beritasosial.response.data.map((beritasosial, key) =>
                  <Grid item xs={12} lg={3}>
                    <Card style={{ maxWidth: '345' }} key={beritasosial.id_berita}>
                      <CardHeader
                        title={WordLimiter(beritasosial.judul_berita, 20)}
                        subheader={beritasosial.timestamp}
                      />
                      <CardMedia
                        style={{ height: '200' }}
                        onClick={event => this.handleClick(event, beritasosial)}
                        //image={require(`./../..${beritaagama.image_berita}`)}
                        image={`${process.env.REACT_APP_APIHOST}/singlefoto/${beritasosial.image_berita}`}
                        title={beritasosial.deskripsi_berita}
                      />
                      <CardContent>
                        <p>
                          {WordLimiter(beritasosial.isi_berita, 80)}
                        </p>
                      </CardContent>
                    </Card>
                  </Grid>
                )}
            </Grid>
          </div>
          <div className="Paging" style={{ margin: 'auto', width: '17%' }}>
            <Pagination
              prev
              next
              first
              last
              ellipsis
              boundaryLinks
              items={this.state.beritasosial.response.total % 10 < 1 ? this.state.beritasosial.response.total / 10 : (this.state.beritasosial.response.total / 10) + 1}
              maxButtons={5}
              activePage={this.state.beritasosial.response.page}
              onSelect={this.handleSelectSosial} />
          </div>
        </TabContainer>}
      </div>
    );
  }
}

ListBerita.propTypes = {
  //classes: PropTypes.object.isRequired,
  beritaagama: PropTypes.object,
  beritahumaniora: PropTypes.object,
  beritapendidikan: PropTypes.object,
  beritasosial: PropTypes.object,
  AgamaActions: PropTypes.func.isRequired,
  HumanioraActions: PropTypes.func.isRequired,
  PendidikanActions: PropTypes.func.isRequired,
  SosialActions: PropTypes.func.isRequired
};

ListBerita.contextTypes = {
  router: React.PropTypes.object.isRequired
}

function MapStateToProps(state) {
  return {
    beritaagama: state.beritaagama,
    beritahumaniora: state.beritahumaniora,
    beritapendidikan: state.beritapendidikan,
    beritasosial: state.beritasosial
  }
}

const MapDispactToProps = dispatch => ({
  AgamaActions: bindActionCreators(AgamaActions, dispatch),
  HumanioraActions: bindActionCreators(HumanioraActions, dispatch),
  PendidikanActions: bindActionCreators(PendidikanActions, dispatch),
  SosialActions: bindActionCreators(SosialActions, dispatch),
  beritaDetailActions: bindActionCreators(beritaDetailActions, dispatch)
})

export default withRouter(connect(MapStateToProps, MapDispactToProps)(ListBerita));