import React, { Component, PropTypes } from 'react';
import * as beritaDetailActions from 'redux-modules/modules/beritadetail';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { Grid, Row, Col } from 'react-bootstrap';

class BeritaDetail extends Component {
  constructor(props) {
    super(props);
    let judul = '';
    let deskripsi = '';
    let isi = '';
    let gambar = '';
    let kategori = '';
    let time = '';
    if (props.beritadetail && props.beritadetail.loaded && props.beritadetail.response) {
      judul = props.beritadetail.response.judul_berita;
      deskripsi = props.beritadetail.response.deskripsi_berita;
      isi = props.beritadetail.response.isi_berita;
      gambar = props.beritadetail.response.image_berita;
      kategori = props.beritadetail.response.nama_kategori_berita;
      time = props.beritadetail.response.timestamp;
    }
    this.state = {
      judul,
      deskripsi,
      isi,
      gambar,
      kategori,
      time,
      loaded: false
    }
  }

  componentWillMount() {
    if (!this.props.beritadetail.loaded) {
      this.props.beritaDetailActions.loadDetailBerita(this.props.match.params.id);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.beritadetail && nextProps.beritadetail.loaded && nextProps.beritadetail.response) {
      console.log(nextProps.beritadetail);
      console.log('berita detail', nextProps.beritadetail.response.data);
      this.setState({ beritadetail: nextProps.beritadetail })
    }
  }

  render() {
    return (
      <div className="container">
        <Grid>
          <Row>
            <Col md={12}>
              <h1 style={{ fontSize: "55px", fontWeight: "400", textAlign: "left"}}>{this.state.judul}</h1>
              <img src={`${process.env.REACT_APP_APIHOST}/singlefoto/${this.state.gambar}`} 
              style={{ width: '50%', height: '50%', display: "block" }}></img>
              <h3 style={{ textAlign: "left"}}>{this.state.deskripsi}</h3>
							<p>{this.state.isi}</p>
            </Col>
          </Row>
        </Grid>
        {/* <p>{this.state.judul}</p>
        <p>{this.state.deskripsi}</p>
        <p>{this.state.isi}</p>
        <img src={`${process.env.REACT_APP_APIHOST}/singlefoto/${this.state.gambar}`} style={{width: '100%'}}></img> */}
      </div>
    )
  }
}

BeritaDetail.propTypes = {
  beritadetail: PropTypes.object,
  beritaDetailActions: PropTypes.func.isRequired
}

BeritaDetail.contextTypes = {
  router: React.PropTypes.object.isRequired
}

function MapStateToProps(state) {
  return {
    beritadetail: state.beritadetail
  }
}

const MapDisptachToProps = dispatch => {
  return {
    beritaDetailActions: bindActionCreators(beritaDetailActions, dispatch)
  }
}

export default withRouter(connect(MapStateToProps, MapDisptachToProps)(BeritaDetail));