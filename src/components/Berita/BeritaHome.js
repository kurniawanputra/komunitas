import React, {Component, PropTypes} from 'react';
import * as beritahomeActions from 'redux-modules/modules/beritahome';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Card, { CardHeader, CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';

class BeritaHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      beritahome: { response: { data: [] } }
    }

    this.handleLengkap = this.handleLengkap.bind(this);
  }

  handleLengkap() {
    window.location.href = `${process.env.REACT_APP_HOST}/berita`;
  }

  componentWillMount() {
    this.props.beritahomeActions.BeritaHome();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.beritahome && nextProps.beritahome.response && nextProps.beritahome.response.data) {
      console.log('fotohome', nextProps.beritahome);
      this.setState({ beritahome: nextProps.beritahome })
    }
  }

  render() {
    return(
      <div className="container">
        <Grid container>
            {this.state.beritahome.response && this.state.beritahome.response.data && this.state.beritahome.response.data.length === 0 &&
              <h1>Tidak ada Data</h1>
            }
            {this.state.beritahome.response && this.state.beritahome.response.data && this.state.beritahome.response.data.length > 0
              && this.state.beritahome.response.data.map((beritahome, key) =>
                <Grid item xs={12} lg={3}>
                  <Card style={{ maxWidth: '345' }} key={beritahome.id_berita}>
                    <CardHeader
                      title={beritahome.judul_berita}
                      subheader={beritahome.timestamp}
                    />
                    <CardMedia
                      style={{ height: '200' }}
                      //image={require(`..${beritaagama.image_berita}`)}
                      image={`${process.env.REACT_APP_APIHOST}/singlefoto/${beritahome.image_berita}`}
                      title={beritahome.deskripsi_berita}
                    />
                    <CardContent>
                      <Typography component="p">
                        {beritahome.isi_berita}
                      </Typography>
                    </CardContent>
                  </Card>
                </Grid>
              )}
        </Grid>
        <button type="button" className="btn btn-primary" style={{marginTop: '10px'}} 
        onClick={this.handleLengkap}>Lihat Selengkapnya</button>
      </div>
    )
  }
}

BeritaHome.propTypes = {
  beritahome: PropTypes.object,
  beritahomeActions: PropTypes.func.isRequired
}

BeritaHome.contextTypes = {
  router: React.PropTypes.object.isRequired
}

function MapStateToProps(state) {
  return {
    beritahome: state.beritahome
  }
}

const MapDispatchToProps = dispatch => ({
  beritahomeActions: bindActionCreators(beritahomeActions, dispatch)
})

export default connect(MapStateToProps, MapDispatchToProps)(BeritaHome);