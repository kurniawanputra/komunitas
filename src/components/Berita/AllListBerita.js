import React, { Component, PropTypes } from 'react';
import { withRouter } from 'react-router-dom';
import { Table, Button, Pagination } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import ButtonMaterial from 'material-ui/Button';
import Delete from 'material-ui-icons/Delete';
import Edit from 'material-ui-icons/Edit';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
} from 'material-ui/Dialog';
import * as ListBeritaActions from 'redux-modules/modules/listberita';
import * as beritaEditActions from 'redux-modules/modules/beritaedit';
import request from 'superagent';
import Exclamation from './../../image/exclamation-mark.png';
import { WordLimiter } from './../../common/LimitKata';

class AllListBerita extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listberita: { response: { data: [] } },
      page: 1,
      activePage: 1,
      selectedBeritaId: null,
      open : false
    }

    this.handleSelect  = this.handleSelect.bind(this);
    this.handleDelete  = this.handleDelete.bind(this);
    this.handleClose   = this.handleClose.bind(this);
    this.handleConfirm = this.handleConfirm.bind(this);
    this.addBerita     = this.addBerita.bind(this);
  }

  addBerita() {
    window.location.href = `${process.env.REACT_APP_HOST}/addberita`;
  }

  handleSelect(page) {
    this.props.ListBeritaActions.ListBeritaAll(page);
  }

  handleDelete(selectedBeritaId) {
    this.setState({ open : true, selectedBeritaId });
    //console.log('id berita', selectedBeritaId);
  }

  handleConfirm(index) {
    const data = {
      id_berita : this.state.selectedBeritaId
    }
    //console.log("delete data", data);
    request.post(`${process.env.REACT_APP_APIHOST}/v1/deleteberita`)
    .send(data)
    .then(success => {
      this.props.ListBeritaActions.DeleteBerita(this.state.selectedBeritaId);
      this.setState({ open: false, selectedBeritaId: null});
    }, error => {
      this.setState({ open: false, selectedBeritaId: null});
    })
    //console.log("data masuk");
  }

  handleClose() {
    this.setState({ open: false, selectedBeritaId: null });
  }

  componentWillMount() {
    this.props.ListBeritaActions.ListBeritaAll(1);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.listberita && nextProps.listberita.response) {
      console.log(nextProps.listberita);
      console.log('nextListBerita', nextProps.listberita.response.data);
      this.setState({ listberita: nextProps.listberita })
    }
  }

  render() {
    const { fullScreen } = this.props;
    return (
      <div className="container">
        <div>
        <h2>List Berita</h2>
        <button type="button" className="btn btn-primary" style={{float: 'right', marginBottom: '10px'}} 
        onClick={this.addBerita}>Tambah Berita</button>
        </div>
        <Table striped bordered condensed hover>
          <thead>
            <tr>
              <th style={{textAlign: "center"}}>No</th>
              <th style={{textAlign: "center"}}>Judul Berita</th>
              <th style={{textAlign: "center"}}>Deskripsi Berita</th>
              <th style={{textAlign: "center"}}>Isi Berita</th>
              <th style={{textAlign: "center"}}>image berita</th>
              <th style={{textAlign: "center"}}>Kategori Berita</th>
              <th style={{textAlign: "center"}}>Edit</th>
              <th style={{textAlign: "center"}}>Delete</th>
            </tr>
          </thead>
          <tbody>
            {this.state.listberita.response && this.state.listberita.response.data && this.state.listberita.response.data.length === 0 &&
              <tr><td colSpan="7" style={{ textAlign: 'center' }}>Tidak ada Data</td></tr>
            }
            {this.state.listberita.response && this.state.listberita.response.data && this.state.listberita.response.data.length > 0 && this.state.listberita.response.data.map((listberita, key) =>
              <tr key={listberita.id_berita}>
                <td>{((this.state.listberita.response.page - 1) * 10) + parseInt(key) + 1}</td>
                <td>{WordLimiter(listberita.judul_berita,15)}</td>
                <td>{WordLimiter(listberita.deskripsi_berita, 20)}</td>
                <td>{WordLimiter(listberita.isi_berita, 30)}</td>
                <td><img src={`${process.env.REACT_APP_APIHOST}/singlefoto/${listberita.image_berita}`} width={50} height={50}></img></td>
                <td>{listberita.id_kategori_berita}</td>
                <td onClick={event => {
                  this.props.beritaEditActions.setEditBerita(listberita);
                  this.props.history.push(`/updateberita/${listberita.id_berita}`);
                }}
                ><Edit /></td>
                <td><ButtonMaterial raised color="default" onClick={() => this.handleDelete(listberita.id_berita)}>
                  <Delete style={{ marginRight: 'theme.spacing.unit' }} />
                </ButtonMaterial></td>
              </tr>
            )}
          </tbody>
          <Dialog
            fullScreen={fullScreen}
            open={this.state.open}
            onClose={this.handleClose}
            aria-labelledby="confirm delete">
            {/* <DialogTitle id="confirm delete">{"Hapus Data"}</DialogTitle> */}
            <DialogContent>
            <img src={Exclamation} width={200} height={200} />
            <br />
              <p style={{fontWeight: "bold"}}>
                Anda yakin mau hapus data?
              </p>
            </DialogContent>
            <div style={{ marginBottom: "10px", marginTop: "-20px"}}>
              <button onClick={this.handleConfirm} 
              style={{marginRight: "3px", padding: "8px", 
              backgroundColor: "#1a75ff", borderRadius: "5px", 
              color: "white", border: "none"}}>Ya, Hapus ini</button>
              <button onClick={this.handleClose} 
              style={{ padding: "8px", color: "white",
              backgroundColor: "#ff1a1a", borderRadius: "5px", border: "none"}}>Tidak</button>
            </div>
          </Dialog>
        </Table>
        <div>
          <div className="Paging" style={{ margin: 'auto', width: '17%' }}>
            <Pagination
              prev
              next
              first
              last
              ellipsis
              boundaryLinks
              items={this.state.listberita.response.total % 10 < 1 ? this.state.listberita.response.total / 10 : (this.state.listberita.response.total / 10) + 1}
              maxButtons={5}
              activePage={this.state.listberita.response.page}
              onSelect={this.handleSelect} />
          </div>
        </div>
      </div>
    )
  }
}

AllListBerita.propsTypes = {
  listberita: PropTypes.object,
  ListBeritaActions: PropTypes.func.isRequired
}

AllListBerita.contextTypes = {
  router: React.PropTypes.object.isRequired
}

function MapStateToProps(state) {
  return {
    listberita: state.listberita
  }
}

const MapsDispatchToProps = dispatch => ({
  ListBeritaActions: bindActionCreators(ListBeritaActions, dispatch),
  beritaEditActions: bindActionCreators(beritaEditActions, dispatch)
})

export default withRouter(connect(MapStateToProps, MapsDispatchToProps)(AllListBerita));