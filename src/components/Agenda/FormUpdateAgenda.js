import React, { Component, PropTypes } from 'react';
import DateTime from 'react-datetime';
import moment, { locale } from 'moment';
import { CircularProgress } from 'material-ui/Progress';
import Button from 'material-ui/Button';
import green from 'material-ui/colors/green';
import Save from 'material-ui-icons/Save';
import Cancel from 'material-ui-icons/Cancel';
import * as agendaEditActions from 'redux-modules/modules/agendaedit';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { Alert } from 'react-bootstrap';
import { UpdateAgenda } from './../../common/agendaAction';

class FormUpdateAgenda extends Component {
  constructor(props) {
    super(props);
    console.log('props', props);
    let
      id_agenda = '',
      desc = '',
      end = moment(),
      judul = '',
      start = moment();
    if (props.agendaedit && props.agendaedit.loaded && props.agendaedit.response) {
      id_agenda = props.agendaedit.response.id_agenda;
      desc = props.agendaedit.response.desc;
      end = new Date(props.agendaedit.response.end_date);
      judul = props.agendaedit.response.title;
      start = new Date(props.agendaedit.response.start_date);
    }
    this.state = {
      id_agenda,
      judul,
      desc,
      start,
      end,
      alertError: false
    }

    this.handleStartDate = this.handleStartDate.bind(this);
    this.handleEndDate = this.handleEndDate.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleAlertDismiss = this.handleAlertDismiss.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.judul === "") {
      //alert("data tidak ada");
      this.timer = setTimeout(() => this.setState({ loading: false }), 2000);
      this.setState({ alertError: true});
    } else {
      this.setState({ loading: true });
      const data = {
        id_agenda : this.state.id_agenda,
        title : this.state.judul,
        start_date : this.state.start,
        end_date : this.state.end,
        desc : this.state.desc
      }
      //console.log(data);
      this.props.UpdateAgenda(data, this.props.history.push);
    }
  }

  handleAlertDismiss() {
    this.setState({ alertError: false });
  }

  handleStartDate(e) {
    const sd = moment(e).format("YYYY-MM-DD HH:m:s")
    this.setState({ start: sd });
  }

  handleEndDate(e) {
    const ed = moment(e).format("YYYY-MM-DD HH:m:s")
    this.setState({ end: ed });
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  timer = undefined;

  componentWillUnmount() {
    clearTimeout(this.timer);
  }


  componentWillMount() {
    if (!this.props.agendaedit.loaded) {
      this.props.agendaEditActions.loadEditAgenda(this.props.match.params.id);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.agendaedit && nextProps.agendaedit.loaded && nextProps.agendaedit.response) {
      console.log(nextProps.agendaedit);
      this.setState({
        id_agenda: nextProps.agendaedit.response.id_agenda,
        desc: nextProps.agendaedit.response.desc,
        end: new Date(nextProps.agendaedit.end_date),
        judul: nextProps.agendaedit.response.title,
        start: new Date(nextProps.agendaedit.start_date)
      })
    }
  }


  render() {
    let { alertError, loading, success } = this.state;
    return (
      <div className="container">
        {alertError && <Alert bsStyle="danger" onDismiss={this.handleAlertDismiss}>
          <h4>mohon lengkapi isi data</h4>
        </Alert>}
        <h2>Form Update Agenda</h2>
        <form className="form-horizontal">
          <div className="form-group">
            <label className="control-label col-sm-2">id agenda</label>
            <div className="col-sm-10">
              <input type="text" disabled name="id_agenda" value={this.state.id_agenda} className="form-control"></input>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Judul</label>
            <div className="col-sm-10">
              <input type="text" name="judul" value={this.state.judul} onChange={this.handleChange} className="form-control" placeholder="Masukkan judul berita"></input>
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Start Date</label>
            <div className="col-sm-10">
              <DateTime open={false} input={true} onChange={this.handleStartDate} value={this.state.start} dateFormat="YYYY-MM-DD" timeFormat={true} />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">End Date</label>
            <div className="col-sm-10">
              <DateTime open={false} input={true} onChange={this.handleEndDate} value={this.state.end} dateFormat="YYYY-MM-DD" timeFormat={true} />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Desc</label>
            <div className="col-sm-10">
              <input type="text" name="desc" value={this.state.desc} onChange={this.handleChange} className="form-control" placeholder="masukkan desc"></input>
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <Button raised color="primary" disabled={loading} onClick={this.handleSubmit}
                style={{
                  backgroundColor: '#86ec65',
                  '&:hover': 'backgroundColor: green[700]', marginRight: '10px'
                }}
              ><Save style={{ marginRight: 'theme.spacing.unit' }} />
                Save</Button>
              {loading && <CircularProgress size={24}
                style={{
                  color: 'green[500]', position: 'absolute', top: '50%',
                  left: '50%', marginTop: '-12', marginLeft: '-40'
                }} />}
              <Button raised color="default">
                <Cancel style={{ marginRight: 'theme.spacing.unit' }} />
                Clear
          </Button>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

FormUpdateAgenda.propTypes = {
  agendaedit: PropTypes.object,
  agendaEditActions: PropTypes.func.isRequired,
  UpdateAgenda: PropTypes.func.isRequired
}

FormUpdateAgenda.contextTypes = {
  router: React.PropTypes.object.isRequired
}

function MapStateToProps(state) {
  return {
    agendaedit: state.agendaedit
  }
}

const MapDispatchToProps = dispatch => {
  return {
    agendaEditActions: bindActionCreators(agendaEditActions, dispatch),
    UpdateAgenda: bindActionCreators(UpdateAgenda, dispatch)
  }
}

export default withRouter(connect(MapStateToProps, MapDispatchToProps)(FormUpdateAgenda));