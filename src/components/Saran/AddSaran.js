import React, { Component } from 'react';
import { CircularProgress } from 'material-ui/Progress';
import { Alert } from 'react-bootstrap';
import Button from 'material-ui/Button';
import green from 'material-ui/colors/green';
import Save from 'material-ui-icons/Save';
import Cancel from 'material-ui-icons/Cancel';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { AddSaranAction } from './../../common/saranAction';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import Slide from 'material-ui/transitions/Slide';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class AddSaran extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nama : '',
      email : '',
      subyek : '',
      pesan : '',
      loading : false,
      success : false,
      alertError : false,
      open : false
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleError  = this.handleError.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
  }

  handleCloseDialog() {
    this.setState({ open :false});
    window.location.href = `${process.env.REACT_APP_HOST}/`;
  }

  handleError() {
    this.setState({ alertError : false});
  }

  handleChange(e) {
    this.setState({ [e.target.name] : e.target.value})
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.nama ==='' || this.state.email ==='' || this.state.subyek ==='' || this.state.pesan === '') {
      this.timer = setTimeout(() => this.setState({ loading: false }), 2000);
      this.setState({ alertError: true});
    } else {
      this.setState({ loading : true});
      const data = {
        nama : this.state.nama,
        email : this.state.email,
        subyek : this.state.subyek,
        pesan : this.state.pesan
      }
      this.props.AddSaranAction(data, this.props.history.push);
      this.setState({ open : true});
    }

  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  timer = undefined;

  render() {
    let { loading, success, alertError } = this.state;

    return(
      <div className="container">
        {alertError && <Alert bsStyle="danger" onDismiss={this.handleError}>
          <h4>Mohon lengkapi Isi Data</h4>
          </Alert>
        }
        <h2>Form Saran</h2>
        <form className="form-horizontal">
          <div className="form-group">
              <label className="control-label col-sm-2">Nama</label>
              <div className="col-sm-10">
                <input type="text" name="nama" value={this.state.nama} onChange={this.handleChange} className="form-control" placeholder="Masukkan nama"></input>
              </div>
          </div>
          <div className="form-group">
              <label className="control-label col-sm-2">Email</label>
              <div className="col-sm-10">
                <input type="text" name="email" value={this.state.email} onChange={this.handleChange} className="form-control" placeholder="Masukkan email"></input>
              </div>
          </div>
          <div className="form-group">
              <label className="control-label col-sm-2">Subyek</label>
              <div className="col-sm-10">
                <input type="text" name="subyek" value={this.state.subyek} onChange={this.handleChange} className="form-control" placeholder="Masukkan subyek"></input>
              </div>
          </div>
          <div className="form-group">
              <label className="control-label col-sm-2">Pesan</label>
              <div className="col-sm-10">
                <textarea rows="5" name="pesan" value={this.state.pesan} onChange={this.handleChange} className="form-control" placeholder="Masukkan pesan"></textarea>
              </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
                <Button raised color="primary" disabled={loading} onClick={this.handleSubmit}
                  style={{backgroundColor : '#86ec65', 
                  '&:hover': 'backgroundColor: green[700]', marginRight: '10px'}}
                  ><Save style={{marginRight : 'theme.spacing.unit'}} />
                  Save</Button>
                  {loading && <CircularProgress size={24} 
                  style={{color: 'green[500]', position: 'absolute', top: '50%',
                  left: '50%', marginTop: '-12', marginLeft: '-40'}} />}
                <Button raised color="default">
                <Cancel style={{marginRight : 'theme.spacing.unit'}} />
                Clear
                </Button>
            </div>
          </div>
        </form>
        <Dialog
          open={this.state.open}
          transition={Transition}
          keepMounted
          onClose={this.handleCloseDialog}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">
            {"Terima kasih"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              Terima kasih atas saran anda, saran anda sangat berguna bagi kami
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCloseDialog} color="primary">
              Tutup
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

AddSaran.propTypes = {
  AddSaranAction: React.PropTypes.func.isRequired
}

AddSaran.contextTypes = {
  router: React.PropTypes.object.isRequired
}

export default withRouter(connect(null, {AddSaranAction})(AddSaran));