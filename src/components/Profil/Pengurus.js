import React, { Component, PropTypes } from "react";
import ExpansionPanel, {
  ExpansionPanelDetails,
  ExpansionPanelSummary,
} from 'material-ui/ExpansionPanel';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import Typography from 'material-ui/Typography';
import Logo from './../../image/logo.jpg';

export default class AllList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: null
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  render() {
    const { expanded } = this.state;
    return (
      <div className="container">
        <div style={{ width: '100%' }}>
          <h2 style={{ textAlign: 'left' }}>Kepengurusan</h2>
          <ExpansionPanel expanded={expanded === 'panel1'} onChange={this.handleChange('panel1')}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography style={{ flexBasis: '33.33%', flexShrink: '0', fontSize: '15px' }}>Ketua</Typography>
              <Typography style={{ fontSize: '15px' }}>Wahyu Rahmadi</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <div style={{ borderTop: '2px #454c4a solid', paddingTop : '10px'}}>
                <div className="kiri" style={{ minWidth: '700px', float: 'left' }}>
                  <div className="form-group">
                    <label className="control-label col-sm-6">Nama Lengkap</label>
                    <label className="control-label col-sm-6">Wahyu Rahmadi</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Alamat</label>
                    <label className="control-label col-sm-6">Muara Bulian</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Pekerjaan</label>
                    <label className="control-label col-sm-6">Swasta</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">TTL</label>
                    <label className="control-label col-sm-6">Jambi, 31 April 1986</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Masa Jabatan</label>
                    <label className="control-label col-sm-6">2016-2017</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Hoby</label>
                    <label className="control-label col-sm-6">Futsal , basket ball</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Status</label>
                    <label className="control-label col-sm-6">Sudah Menikah</label>
                  </div>
                </div>
                <div className="kanan" style={{ float: 'right', minWidth: '350px' }}>
                  <img className="thumbnail" src={Logo} width={280} height={227} style={{ display: 'block', marginLeft: 'auto', marginRight: 'auto' }} />
                </div>
              </div>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel expanded={expanded === 'panel2'} onChange={this.handleChange('panel2')}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography style={{ flexBasis: '33.33%', flexShrink: '0', fontSize: '15px' }}>Wakil Ketua</Typography>
              <Typography style={{ fontSize: '15px' }}>Wahyu Rahmadi</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <div style={{ borderTop: '2px #454c4a solid', paddingTop : '10px'}}>
                <div className="kiri" style={{ minWidth: '700px', float: 'left' }}>
                  <div className="form-group">
                    <label className="control-label col-sm-6">Nama Lengkap</label>
                    <label className="control-label col-sm-6">Wahyu Rahmadi</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Alamat</label>
                    <label className="control-label col-sm-6">Muara Bulian</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Pekerjaan</label>
                    <label className="control-label col-sm-6">Swasta</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">TTL</label>
                    <label className="control-label col-sm-6">Jambi, 31 April 1986</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Masa Jabatan</label>
                    <label className="control-label col-sm-6">2016-2017</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Hoby</label>
                    <label className="control-label col-sm-6">Futsal , basket ball</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Status</label>
                    <label className="control-label col-sm-6">Sudah Menikah</label>
                  </div>
                </div>
                <div className="kanan" style={{ float: 'right', minWidth: '350px' }}>
                  <img className="thumbnail" src={Logo} width={280} height={227} style={{ display: 'block', marginLeft: 'auto', marginRight: 'auto' }} />
                </div>
              </div>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel expanded={expanded === 'panel3'} onChange={this.handleChange('panel3')}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography style={{ flexBasis: '33.33%', flexShrink: '0', fontSize: '15px' }}>Bendahara</Typography>
              <Typography style={{ fontSize: '15px' }}>Wahyu Rahmadi</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <div style={{ borderTop: '2px #454c4a solid', paddingTop : '10px'}}>
                <div className="kiri" style={{ minWidth: '700px', float: 'left' }}>
                  <div className="form-group">
                    <label className="control-label col-sm-6">Nama Lengkap</label>
                    <label className="control-label col-sm-6">Wahyu Rahmadi</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Alamat</label>
                    <label className="control-label col-sm-6">Muara Bulian</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Pekerjaan</label>
                    <label className="control-label col-sm-6">Swasta</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">TTL</label>
                    <label className="control-label col-sm-6">Jambi, 31 April 1986</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Masa Jabatan</label>
                    <label className="control-label col-sm-6">2016-2017</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Hoby</label>
                    <label className="control-label col-sm-6">Futsal , basket ball</label>
                  </div>
                  <br />
                  <div className="form-group">
                    <label className="control-label col-sm-6">Status</label>
                    <label className="control-label col-sm-6">Sudah Menikah</label>
                  </div>
                </div>
                <div className="kanan" style={{ float: 'right', minWidth: '350px' }}>
                  <img className="thumbnail" src={Logo} width={280} height={227} style={{ display: 'block', marginLeft: 'auto', marginRight: 'auto' }} />
                </div>
              </div>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
      </div>
    )
  }
}