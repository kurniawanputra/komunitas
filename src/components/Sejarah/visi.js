import React, { Component } from 'react';

export default class visi extends Component {
  render() {
    return (
      <div style={{ backgroundColor: '#74a0e8', minHeight: '270px', padding: '30px', color: 'white', marginBottom: '10px' }}>
        <div className="row">
          <div className="col-md-3">
            <h2 style={{fontSize: 40, fontWeight: 'bold'}}>Visi</h2>
          </div>
          <div className="col-md-9">
            <p style={{fontSize: 20}}>1. lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>
            <p style={{fontSize: 20}}>2. lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>
            <p style={{fontSize: 20}}>3. lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>
          </div>
        </div>
        <br />
        <br />
        <div className="row">
          <div className="col-md-3">
            <h2 style={{fontSize: 40, fontWeight: 'bold'}}>Misi</h2>
          </div>
          <div className="col-md-9">
            <p style={{fontSize: 20}}>1. lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>
            <p style={{fontSize: 20}}>2. lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>
            <p style={{fontSize: 20}}>3. lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>
          </div>
        </div>
      </div>
    )
  }
}