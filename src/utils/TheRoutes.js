import FirstPage from 'containers/FirstPage/FirstPage';
import NotFound from 'containers/NotFound/NotFound';
import SecondPage from 'containers/SecondPage/SecondPage';
import Form1 from 'containers/Form/form1';
import Form2 from 'containers/Form/form2';
import Profil from 'containers/Profil/Profil';
import Foto from 'containers/Foto/Foto';
import Berita from 'containers/Berita/Berita';
import Hubungi from 'containers/Hubungi/Hubungi';
import Agenda from 'containers/Agenda/Agenda';
import AddAgenda from 'containers/Agenda/AddAgenda';
import AddGallery from 'containers/Form/FormInputGallery';
import ListAgenda from 'containers/Agenda/ListAgenda';
import AddBerita from 'containers/Berita/AddBerita';
import UpdateAgenda from 'containers/Agenda/UpdateAgenda';
import ListBerita from 'containers/Berita/TableBerita';
import UpdateBerita from 'containers/Berita/UpdateBerita';
import AddUser from 'containers/User/FormAddUser';
import ListUser from 'containers/User/FormListUser';
import UpdateUser from 'containers/User/FormUpdateUser';
import AddSaran from 'containers/Saran/FormAddSaran';
import Home from 'containers/Home/home';
import BeritaDetail from 'containers/Berita/FormDetailBerita';
import Login from 'containers/User/FormLogin';

const TheRoutes = [
  {
    path      : '/',
    exact     : true,
    component : Home,
    authType  :'public',
  },
  {
    path      : '/second',
    component : SecondPage,
  },
  {
    path      : '/form1',
    component : Form1
  },
  {
    path      : '/form2',
    component : Form2
  },
  {
    path      : '/profil',
    component : Profil,
    authType  :'public',
  },
  {
    path      : '/foto',
    component : Foto,
    authType  :'public',
  },
  {
    path      : '/berita',
    component : Berita,
    authType  :'public',
  },
  {
    path      : '/hubungi',
    component : Hubungi,
    authType  :'public',
  },
  {
    path      : '/agenda',
    component : Agenda,
    authType  :'public',
  },
  {
    path      : '/addagenda',
    component : AddAgenda,
    authType  :'loggedin',
  },
  {
    path      : '/addgallery',
    component : AddGallery,
    authType  :'loggedin',
  },
  {
    path      : '/listagenda',
    component : ListAgenda,
    authType  :'loggedin',
  },
  {
    path      : '/addberita',
    component : AddBerita,
    authType  :'loggedin',
  },
  {
    path      : '/updateagenda/:id',
    component : UpdateAgenda,
    authType  :'loggedin',
  },
  {
    path      : '/listberita',
    component : ListBerita,
    authType  :'loggedin',
  },
  {
    path      : '/updateberita/:id',
    component : UpdateBerita,
    authType  :'loggedin',
  },
  {
    path      : '/adduser',
    component : AddUser,
    authType  :'loggedin',
  },
  {
    path      : '/listuser',
    component : ListUser,
    authType  :'loggedin'
  },
  {
    path      : '/updateuser/:id',
    component : UpdateUser,
    authType  :'loggedin',
  },
  {
    path      : '/addsaran',
    component : AddSaran,
    authType  :'loggedin',
  },
  {
    path      : '/home',
    component : Home,
    authType  :'public',
  },
  {
    path      : '/beritadetail/:id',
    component : BeritaDetail,
    authType  :'public',
  },
  {
    path      : '/login',
    component : Login,
    authType  :'notloggedin',
  },
  {
    component: NotFound
  }
];

export default TheRoutes;