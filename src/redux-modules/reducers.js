import { combineReducers } from 'redux'
import agenda from './modules/agenda';
import agendaedit from './modules/agendaedit';
import user from './modules/user'
import berita from './modules/berita';
import beritaagama from './modules/beritaagama';
import beritahumaniora from './modules/beritahumaniora';
import beritapendidikan from './modules/beritapendidikan';
import beritasosial from './modules/beritasosial';
import gallery from './modules/gallery';
import listberita from './modules/listberita';
import beritaedit from './modules/beritaedit';
import listuser from './modules/listuser';
import useredit from './modules/useredit';
import fotohome from './modules/fotohome';
import beritahome from './modules/beritahome';
import beritadetail from './modules/beritadetail';
import auth from './modules/auth';


export default combineReducers({
  agenda,
  agendaedit,
  berita,
  user,
  beritaagama,
  beritahumaniora,
  beritapendidikan,
  beritasosial,
  gallery,
  listberita,
  beritaedit,
  listuser,
  useredit,
  fotohome,
  beritahome,
  beritadetail,
  auth
})
