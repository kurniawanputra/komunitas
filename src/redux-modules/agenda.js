import {
  LOAD_LIST_AGENDA,
  LOAD_FAIL_LIST_AGENDA,
  LOAD_SUCCESS_LIST_AGENDA,
  LOAD_SUCCESS_NEXT_AGENDA,
  LOAD_SUCCESS_PREV_AGENDA
} from './../action/types';


const initialState = {
	loaded: false
};

export default (state = initialState, action = {}) => {
  	switch (action.type) {
		case LOAD_LIST_AGENDA:
			return {
				loaded: false,
				loading: true,
			};
    case LOAD_SUCCESS_LIST_AGENDA:
      return {
        ...state,
        loaded: true,
        loading: false,
        response: action.result
      }
    case LOAD_FAIL_LIST_AGENDA:
      return {
        loaded: true,
        loading: false,
        response: action.error
      }
    case LOAD_SUCCESS_NEXT_AGENDA:
      return {
        ...state,
        loaded: true,
        loading: false,
        response: action.result
      }
    case LOAD_SUCCESS_PREV_AGENDA: 
      return {
        ...state,
        loaded: true,
        loading: false,
        response: action.result
      }
    default: return state;
  }
}