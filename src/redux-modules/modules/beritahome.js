import {
  LOAD_LIST_BERITAHOME,
  LOAD_SUCCESS_LIST_BERITAHOME,
  LOAD_FAIL_LIST_BERITAHOME
} from './../../action/types';

const initialState = {
  loaded : false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_LIST_BERITAHOME:
      return {
        loaded : false,
        loading : true
      };
    case LOAD_SUCCESS_LIST_BERITAHOME:
      return {
        ...state,
        loaded : true,
        loading : false,
        response : action.result
      }
    case LOAD_FAIL_LIST_BERITAHOME:
      return {
        loaded : true,
        loading : false,
        response : action.error        
      }
    default: return state;
  }
}

export function BeritaHome(data) {
  return {
    types: [LOAD_LIST_BERITAHOME, LOAD_SUCCESS_LIST_BERITAHOME, LOAD_FAIL_LIST_BERITAHOME],
    promise: client => client.get(`${process.env.REACT_APP_APIHOST}/v1/beritahome`, data)
  }
}