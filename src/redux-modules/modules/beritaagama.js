import {
  LOAD_LIST_AGAMA,
  LOAD_SUCCESS_LIST_AGAMA,
  LOAD_FAIL_LIST_AGAMA
} from './../../action/types';

const initialState = {
  loaded : true
};

export default (state = initialState, action = {}) => {
  switch(action.type) {
    case LOAD_LIST_AGAMA : 
      return {
        loaded : false,
        loading : true
      }
    case LOAD_SUCCESS_LIST_AGAMA :
      return {
        ...state,
        loaded : true,
        loading : false,
        response : action.result
      }
    case LOAD_FAIL_LIST_AGAMA : 
      return {
        loaded : true,
        loading : false,
        response : action.error
      }
    default : return state;
  }
}

export function ListAgama(page) {
  return {
    types: [LOAD_LIST_AGAMA, LOAD_SUCCESS_LIST_AGAMA, LOAD_FAIL_LIST_AGAMA],
    promise: client => client.get(`${process.env.REACT_APP_APIHOST}/v1/listberitaagama/${page}`)
  }
}

