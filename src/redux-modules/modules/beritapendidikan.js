import {
  LOAD_LIST_PENDIDIKAN,
  LOAD_SUCCESS_LIST_PENDIDIKAN,
  LOAD_FAIL_LIST_PENDIDIKAN
} from './../../action/types';

const initialState = {
  loaded : true
};

export default (state = initialState, action = {}) => {
  switch(action.type) {
    case LOAD_LIST_PENDIDIKAN : 
      return {
        loaded : false,
        loading : true
      }
    case LOAD_SUCCESS_LIST_PENDIDIKAN :
      return {
        ...state,
        loaded : true,
        loading : false,
        response : action.result
      }
    case LOAD_FAIL_LIST_PENDIDIKAN : 
      return {
        loaded : true,
        loading : false,
        response : action.error
      }
    default : return state;
  }
}

export function ListPendidikan(page) {
  return {
    types: [LOAD_LIST_PENDIDIKAN, LOAD_SUCCESS_LIST_PENDIDIKAN, LOAD_FAIL_LIST_PENDIDIKAN],
    promise: client => client.get(`${process.env.REACT_APP_APIHOST}/v1/listberitapendidikan/${page}`)
  }
}