import {
  LOAD_LIST_FOTOHOME,
  LOAD_SUCCESS_LIST_FOTOHOME,
  LOAD_FAIL_LIST_FOTOHOME
} from './../../action/types';

const initialState = {
  loaded : false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_LIST_FOTOHOME:
      return {
        loaded : false,
        loading : true
      };
    case LOAD_SUCCESS_LIST_FOTOHOME:
      return {
        ...state,
        loaded : true,
        loading : false,
        response : action.result
      }
    case LOAD_FAIL_LIST_FOTOHOME:
      return {
        loaded : true,
        loading : false,
        response : action.error        
      }
    default: return state;
  }
}

export function FotoHome(data) {
  return {
    types: [LOAD_LIST_FOTOHOME, LOAD_SUCCESS_LIST_FOTOHOME, LOAD_FAIL_LIST_FOTOHOME],
    promise: client => client.get(`${process.env.REACT_APP_APIHOST}/v1/galleryhome`, data)
  }
}