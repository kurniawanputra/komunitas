import {
  LOAD_LIST_HUMANIORA,
  LOAD_SUCCESS_LIST_HUMANIORA,
  LOAD_FAIL_LIST_HUMANIORA
} from './../../action/types';

const initialState = {
  loaded : true
};

export default (state = initialState, action = {}) => {
  switch(action.type) {
    case LOAD_LIST_HUMANIORA : 
      return {
        loaded : false,
        loading : true
      }
    case LOAD_SUCCESS_LIST_HUMANIORA :
      return {
        ...state,
        loaded : true,
        loading : false,
        response : action.result
      }
    case LOAD_FAIL_LIST_HUMANIORA : 
      return {
        loaded : true,
        loading : false,
        response : action.error
      }
    default : return state;
  }
}

export function ListHumaniora(page) {
  return {
    types: [LOAD_LIST_HUMANIORA, LOAD_SUCCESS_LIST_HUMANIORA, LOAD_FAIL_LIST_HUMANIORA],
    promise: client => client.get(`${process.env.REACT_APP_APIHOST}/v1/listberitahumaniora/${page}`)
  }
}