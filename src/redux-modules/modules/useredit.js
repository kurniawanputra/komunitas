import {
  LOAD_EDIT_USER,
  LOAD_SUCCESS_EDIT_USER,
  LOAD_FAIL_EDIT_USER
} from './../../action/types';

const initialState = {
  loaded: false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_EDIT_USER:
      return {
        loaded: false,
        loading: true,
      };
    case LOAD_SUCCESS_EDIT_USER:
      return {
        ...state,
        loaded: true,
        loading: false,
        response: action.result
      }
    case LOAD_FAIL_EDIT_USER:
      return {
        loaded: true,
        loading: false,
        response: action.error
      }
    default: return state;
  }
}

export function setEditUser(result) {
  return {
    type: LOAD_SUCCESS_EDIT_USER,
    result
  }
}

export function loadEditUser(id) {
  return {
    types: [LOAD_EDIT_USER, LOAD_SUCCESS_EDIT_USER, LOAD_FAIL_EDIT_USER],
    promise: client => client.get(`${process.env.REACT_APP_APIHOST}/v1/finduser/` + id)
  }
}