import {
  LOAD_EDIT_BERITA,
  LOAD_SUCCESS_EDIT_BERITA,
  LOAD_FAIL_EDIT_BERITA
} from './../../action/types';

const initialState = {
  loaded: false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_EDIT_BERITA:
      return {
        loaded: false,
        loading: true,
      };
    case LOAD_SUCCESS_EDIT_BERITA:
      return {
        ...state,
        loaded: true,
        loading: false,
        response: action.result
      }
    case LOAD_FAIL_EDIT_BERITA:
      return {
        loaded : true,
        loading : false,
        response : action.error
      }
    default : return state;
  }
}

export function setEditBerita(result) {
  return {
    type : LOAD_SUCCESS_EDIT_BERITA,
    result
  }
}

export function loadEditBerita(id) {
  return {
    types : [LOAD_EDIT_BERITA, LOAD_SUCCESS_EDIT_BERITA, LOAD_FAIL_EDIT_BERITA],
    promise: client => client.get(`${process.env.REACT_APP_APIHOST}/v1/findberita/` + id)
  }
}