import {
  LOAD_LIST_TABLE_BERITA,
  LOAD_SUCCESS_LIST_TABLE_BERITA,
  LOAD_FAIL_LIST_TABLE_BERITA,
  LOAD_SUCCESS_DELETE_BERITA,
  LOAD_FAIL_DELETE_BERITA
} from './../../action/types';

const initialState = {
  loaded: false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_LIST_TABLE_BERITA:
      return {
        loaded: false,
        loading: true
      }
    case LOAD_SUCCESS_LIST_TABLE_BERITA:
      return {
        ...state,
        loaded: true,
        loading: false,
        response: action.result
      }
    case LOAD_FAIL_LIST_TABLE_BERITA:
      return {
        loaded: true,
        loading: false,
        response: action.error
      }
    case LOAD_SUCCESS_DELETE_BERITA:
      let { data } = state.response;
      console.log('action', action);
      data = data.filter(d => {
        return d.id_berita !== action.id
      });
      console.log('data', data);
      const response = {
        ...state.response,
        data: [...data]
      }
      return {
        ...state,
        loaded: true,
        loading : false,
        response
      }
    default: return state;
  }
}

export function ListBeritaAll(page) {
  return {
    types : [LOAD_LIST_TABLE_BERITA, LOAD_SUCCESS_LIST_TABLE_BERITA, LOAD_FAIL_LIST_TABLE_BERITA],
    promise: client => client.get(`${process.env.REACT_APP_APIHOST}/v1/listberitaall/${page}`)
  }
}

export function DeleteBerita(id) {
  return {
    type : LOAD_SUCCESS_DELETE_BERITA,
    id
  }
}