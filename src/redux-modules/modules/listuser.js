import {
  LOAD_LIST_USER,
  LOAD_SUCCESS_LIST_USER,
  LOAD_FAIL_LIST_USER,
  LOAD_SUCCESS_DELETE_USER
} from './../../action/types';

const initialState = {
  loaded: false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_LIST_USER:
      return {
        loaded: false,
        loading: true
      }
    case LOAD_SUCCESS_LIST_USER:
      return {
        ...state,
        loaded: true,
        loading: false,
        response: action.result
      }
    case LOAD_FAIL_LIST_USER:
      return {
        loaded: true,
        loading: false,
        response: action.error
      }
    case LOAD_SUCCESS_DELETE_USER:
      let { data } = state.response;
      console.log('action', action);
      data = data.filter(d => {
        return d.id_user !== action.id
      });
      console.log('data', data);
      const response = {
        ...state.response,
        data: [...data]
      }
      return {
        ...state,
        loaded: true,
        loading: false,
        response
      }
    default: return state;
  }
}

export function ListUser(page) {
  return {
    types: [LOAD_LIST_USER, LOAD_SUCCESS_LIST_USER, LOAD_FAIL_LIST_USER],
    promise: client => client.get(`${process.env.REACT_APP_APIHOST}/v1/listuser/${page}`)
  }
}

export function DeleteUser(id) {
  return {
    type: LOAD_SUCCESS_DELETE_USER,
    id
  }
}