import {
  LOAD_LIST_AGENDA,
  LOAD_FAIL_LIST_AGENDA,
  LOAD_SUCCESS_LIST_AGENDA,
  LOAD_SUCCESS_NEXT_AGENDA,
  LOAD_SUCCESS_PREV_AGENDA,
  LOAD_SUCCESS_ADD_AGENDA,
  LOAD_FAIL_ADD_AGENDA,
  LOAD_SUCCESS_DELETE_AGENDA,
  LOAD_FAIL_DELETE_AGENDA
} from './../../action/types';


const initialState = {
  loaded: false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_LIST_AGENDA:
      return {
        loaded: false,
        loading: true,
      };
    case LOAD_SUCCESS_LIST_AGENDA:
      return {
        ...state,
        loaded: true,
        loading: false,
        response: action.result
      }
    case LOAD_FAIL_LIST_AGENDA:
      return {
        loaded: true,
        loading: false,
        response: action.error
      }
    case LOAD_SUCCESS_NEXT_AGENDA:
      return {
        ...state,
        loaded: true,
        loading: false,
        response: action.result
      }
    case LOAD_SUCCESS_PREV_AGENDA:
      return {
        ...state,
        loaded: true,
        loading: false,
        response: action.result
      }
    case LOAD_SUCCESS_ADD_AGENDA:
      return {
        ...state,
        loaded: true,
        loading: false,
        response: action.result
      }
    case LOAD_FAIL_ADD_AGENDA:
      return {
        loaded: true,
        loading: false,
        response: action.error
      }
    case LOAD_SUCCESS_DELETE_AGENDA:
      let { data } = state.response;
      data = data.filter(d => {
        return d.id_agenda !== action.id
      });
      const response = {
        ...state.response,
        data: [...data]
      }
      return {
        ...state,
        loaded : true,
        loading : false,
        response
      }
    default: return state;
  }
}

export function ListAgenda(page) {
  return {
    types: [LOAD_LIST_AGENDA, LOAD_SUCCESS_LIST_AGENDA, LOAD_FAIL_LIST_AGENDA],
    promise: client => client.get(`${process.env.REACT_APP_APIHOST}/v1/listagenda/${page}`)
  }
}

export function AllAgenda(data) {
  return {
    types: [LOAD_LIST_AGENDA, LOAD_SUCCESS_LIST_AGENDA, LOAD_FAIL_LIST_AGENDA],
    promise: client => client.get(`${process.env.REACT_APP_APIHOST}/v1/listagendaall`, data)
  }
}

export function DeleteAgenda(id) {
  return {
    type: LOAD_SUCCESS_DELETE_AGENDA,
    id
  }
}

// export function AddAgenda(data) {
//   return {
//     types: [LOAD_LIST_AGENDA, LOAD_SUCCESS_ADD_AGENDA, LOAD_FAIL_ADD_AGENDA],
//     promise: client => client.post(`${process.env.REACT_APP_APIHOST}/v1/addagenda`, data)
//   }
// }
