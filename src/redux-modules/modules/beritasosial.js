import {
  LOAD_LIST_SOSIAL,
  LOAD_SUCCESS_LIST_SOSIAL,
  LOAD_FAIL_LIST_SOSIAL
} from './../../action/types';

const initialState = {
  loaded : true
};

export default (state = initialState, action = {}) => {
  switch(action.type) {
    case LOAD_LIST_SOSIAL : 
      return {
        loaded : false,
        loading : true
      }
    case LOAD_SUCCESS_LIST_SOSIAL :
      return {
        ...state,
        loaded : true,
        loading : false,
        response : action.result
      }
    case LOAD_FAIL_LIST_SOSIAL : 
      return {
        loaded : true,
        loading : false,
        response : action.error
      }
    default : return state;
  }
}

export function ListSosial(page) {
  return {
    types: [LOAD_LIST_SOSIAL, LOAD_SUCCESS_LIST_SOSIAL, LOAD_FAIL_LIST_SOSIAL],
    promise: client => client.get(`${process.env.REACT_APP_APIHOST}/v1/listberitasosial/${page}`)
  }
}