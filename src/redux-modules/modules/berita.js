import {
  LOAD_SUCCESS_ADD_BERITA,
  LOAD_FAIL_ADD_BERITA
} from './../../action/types';

const initialState = {
  loaded: false
};

export default (state=initialState, action ={}) => {
  switch(action.type) {
    case LOAD_SUCCESS_ADD_BERITA:
      return {
        ...state,
        loaded : true,
        loading : false,
        response : action.result
      }
    case LOAD_FAIL_ADD_BERITA:
      return {
        loaded : true,
        loading : false,
        response : action.error
      }
    default : return state;

  }
}