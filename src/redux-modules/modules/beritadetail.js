import {
  LOAD_DETAIL_BERITA,
  LOAD_SUCCESS_DETAIL_BERITA,
  LOAD_FAIL_DETAIL_BERITA
} from './../../action/types';

const initialState = {
  loaded : false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_DETAIL_BERITA:
      return {
        loaded : false,
        loading : true
      };
    case LOAD_SUCCESS_DETAIL_BERITA:
      return {
        ...state,
        loaded : true,
        loading : false,
        response : action.result
      }
    case LOAD_FAIL_DETAIL_BERITA:
      return {
        loaded : true,
        loading : false,
        response : action.error
      }
    default : return state;
  }
}

export function setDetailBerita(result) {
  return {
    type : LOAD_SUCCESS_DETAIL_BERITA,
    result
  }
} 

export function loadDetailBerita(id) {
  return {
    types : [LOAD_DETAIL_BERITA, LOAD_SUCCESS_DETAIL_BERITA, LOAD_FAIL_DETAIL_BERITA],
    promise: client => client.get(`${process.env.REACT_APP_APIHOST}/v1/findberita/` + id)
  }
}