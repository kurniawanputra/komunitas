import {
  LOAD_EDIT_AGENDA,
  LOAD_SUCCESS_EDIT_AGENDA,
  LOAD_FAIL_EDIT_AGENDA
} from './../../action/types';

const initialState = {
  loaded: false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_EDIT_AGENDA:
      return {
        loaded: false,
        loading: true,
      };
    case LOAD_SUCCESS_EDIT_AGENDA:
      return {
        ...state,
        loaded: true,
        loading: false,
        response: action.result
      }
    case LOAD_FAIL_EDIT_AGENDA:
      return {
        loaded: true,
        loading: false,
        response: action.error
      }
    default: return state;
  }
}

export function setEditAgenda(result) {
  return {
    type: LOAD_SUCCESS_EDIT_AGENDA,
    result
  }
}

export function loadEditAgenda(id) {
  return {
    types: [LOAD_EDIT_AGENDA, LOAD_SUCCESS_EDIT_AGENDA, LOAD_FAIL_EDIT_AGENDA],
    promise: client => client.get(`${process.env.REACT_APP_APIHOST}/v1/findagenda/` + id)
  }
}