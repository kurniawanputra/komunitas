/* eslint-disable */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App/App';
import registerServiceWorker from './registerServiceWorker';
import { SheetsRegistry } from 'react-jss/lib/jss';

import './global-styles/index.css';
// import '!style-loader!css-loader!./global-styles/index.css';

import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import { create } from 'jss';
import preset from 'jss-preset-default';

import ApiClient from './utils/ApiClient';
import configureStore from './redux-modules/create';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import createGenerateClassName from 'material-ui/styles/createGenerateClassName';
import { green, red } from 'material-ui/colors';

  // Create a sheetsRegistry instance.
  const sheetsRegistry = new SheetsRegistry();

  // Create a theme instance.
  const theme = createMuiTheme({
    palette: {
      primary: green,
      accent: red,
      type: 'light',
    },
  });

  const jss = create(preset());
  // const jss = create({ plugins: [...preset().plugins, rtl()] }); // in-case you're supporting rtl

  jss.options.createGenerateClassName = createGenerateClassName;

// Let the reducers handle initial state
const initialState = {};
const client = new ApiClient();
const store = configureStore(client, initialState);
ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <MuiThemeProvider theme={theme} sheetsManager={new Map()}>
        <App initialData={window.DATA} />
      </MuiThemeProvider>
    </BrowserRouter>
  </Provider>, document.getElementById('root')
);
registerServiceWorker();
