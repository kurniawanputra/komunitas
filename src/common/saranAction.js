import axios from 'axios';
import {
  LOAD_SUCCESS_ADD_SARAN,
  LOAD_FAIL_ADD_SARAN
} from './../action/types';

export function setAddSaran(result) {
  return {
    type : LOAD_SUCCESS_ADD_SARAN,
    result
  }
}

export function errorAddSaran(error) {
  return {
    type : LOAD_FAIL_ADD_SARAN,
    error
  }
}

export function AddSaranAction(data) {
  return dispatch => {
    return axios.post(`${process.env.REACT_APP_APIHOST}/v1/addsaran`, data)
    .then(res => {
      dispatch(setAddSaran(res.data));
      //callback('/');
    }).catch(error => {
      console.log(error);
      dispatch(errorAddSaran(error));
    });
  }
}