import axios from 'axios';
import setAuthorizationToken from '../common/setAuthorizationToken';
import jwtDecode from 'jwt-decode';
import {
  SET_CURRENT_USER,
  LOAD_SUCCESS_LOGIN,
  LOAD_FAIL_LOGIN
} from './../action/types';

export function setCurrentUser(user) {
  return {
    type : SET_CURRENT_USER,
    user
  }
}

export function setSuccessLogin(result) {
  return {
    type : LOAD_SUCCESS_LOGIN,
    result
  }
}

export function setFailedLogin(error) {
  return {
    type : LOAD_FAIL_LOGIN,
    error
  }
}

export function Login(data, callback) {
  return dispatch => {
    return axios.post(`${process.env.REACT_APP_APIHOST}/v1/checklogin`, data)
    .then(res => {
      const jwt_token = res.data.token;
      console.log('token', jwt_token);
      localStorage.setItem('token', jwt_token);
      setAuthorizationToken(jwt_token);
      dispatch(setCurrentUser(jwtDecode(jwt_token)));
      callback('/home');
    })
    .catch(error => {
      dispatch(setFailedLogin(error));
    })
  }
}

export function Logout(data) {
  return dispatch => {
    localStorage.removeItem('token');
    setAuthorizationToken(false);
    dispatch(setCurrentUser({}));
  }
}

