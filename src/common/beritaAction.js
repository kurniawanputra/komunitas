import axios from 'axios';
import {
  LOAD_SUCCESS_ADD_BERITA,
  LOAD_FAIL_ADD_BERITA,
  LOAD_SUCCESS_UPDATE_BERITA,
  LOAD_FAIL_UPDATE_BERITA
} from './../../src/action/types';

export function setAddBerita(result) {
  return {
    type : LOAD_SUCCESS_ADD_BERITA,
    result
  }
}

export function setUpdateBerita(result) {
  return {
    type : LOAD_SUCCESS_UPDATE_BERITA,
    result
  }
}

export function errorAddBerita(error) {
  return {
    type : LOAD_FAIL_ADD_BERITA,
    error
  }
}

export function errorUpdateBerita(error) {
  return {
    type : LOAD_FAIL_UPDATE_BERITA,
    error
  }
}

export function AddBerita(data, callback) {
  return dispatch => {
    return axios.post(`${process.env.REACT_APP_APIHOST}/v1/addberita`, data)
      .then(res => {
        console.log(data);
        dispatch(setAddBerita(res.data));
        callback('/berita');
      })
      .catch(error => {
        console.log(error);
        dispatch(errorAddBerita(error));
      });
  }
}

export function UpdateBerita(data, callback) {
  return dispatch => {
    return axios.post(`${process.env.REACT_APP_APIHOST}/v1/updateberita`, data)
      .then(res => {
        console.log(data);
        dispatch(setUpdateBerita(res.data));
        callback('/listberita');
      })
      .catch(error => {
        console.log(error);
        dispatch(errorUpdateBerita(error));
      })
  }
}