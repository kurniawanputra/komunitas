export function WordLimiter(text, limit) {
  const textArr = text.split(' ');
  let newText = '';
  let isEllipsis = false;
  textArr.forEach(el => {
    if (newText.length > limit) {
      return;
    }
    isEllipsis = true;
    newText = `${newText}${el} `;
  });

  newText = newText.trim();
  if (isEllipsis) {
    newText = `${newText} ...`;
  }
  return newText;
}