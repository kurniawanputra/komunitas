import axios from 'axios';
import {
  LOAD_SUCCES_ADD_GALLERY,
  LOAD_FAIL_ADD_GALLERY
} from './../../src/action/types';

export function setAddGallery(result) {
  return {
    type: LOAD_SUCCES_ADD_GALLERY,
    result
  }
}

export function errorAddGallery(error) {
  return {
    type: LOAD_FAIL_ADD_GALLERY,
    error
  }
}

var config = {
  headers: {'Content-Type': 'multipart/form-data'}
};

export function AddGallery(data, callback) {
  return dispatch => {
    return axios.post(`${process.env.REACT_APP_APIHOST}/v1/addgallery`, data, config)
    .then(res => {
      console.log(data);
      dispatch(setAddGallery(res.data));
      callback('/');
    }).catch(error => {
      console.log(error);
      dispatch(errorAddGallery(error));
    });
  }
}