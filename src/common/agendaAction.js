import axios from 'axios';
import {
  LOAD_SUCCESS_ADD_AGENDA,
  LOAD_FAIL_ADD_AGENDA,
  LOAD_SUCCESS_LIST_AGENDA,
  LOAD_FAIL_LIST_AGENDA,
  LOAD_LIST_AGENDA,
  LOAD_SUCCESS_UPDATE_AGENDA,
  LOAD_FAIL_UPDATE_AGENDA
} from './../action/types';

export function setAddAgenda(result) {
  return {
    type : LOAD_SUCCESS_ADD_AGENDA,
    result
  }
}

export function errorAddAgenda(error) {
  return {
    type : LOAD_FAIL_ADD_AGENDA,
    error
  }
}

export function setUpdateAgenda(result) {
  return {
    type : LOAD_SUCCESS_UPDATE_AGENDA,
    result
  }
}

export function errorUpdateAgenda(error) {
  return {
    type : LOAD_FAIL_UPDATE_AGENDA,
    error
  }
}

export function setListAgenda(result) {
  return {
    type : LOAD_SUCCESS_LIST_AGENDA,
    result
  }
}

export function errorListAgenda(error) {
  return {
    type : LOAD_FAIL_LIST_AGENDA,
    error
  }
}

export function loadListAgenda(result) {
  return {
      type : LOAD_LIST_AGENDA,
  };
}


export function ListAgenda(page) {
  return dispatch => {
    dispatch(loadListAgenda());
    return axios.get(`${process.env.REACT_APP_APIHOST}/v1/listagenda/${page}`)
      .then(res => {
        dispatch(setListAgenda(res.data));
      })
      .catch(error => {
        dispatch(errorListAgenda(error));
      })
  }
}

export function AddAgenda(data) {
  return dispatch => {
    return axios.post(`${process.env.REACT_APP_APIHOST}/v1/addagenda`, data)
    .then(res => {
      dispatch(setAddAgenda(res.data));
    }).catch(error => {
      console.log(error);
      dispatch(errorAddAgenda(error));
    });
  }
}

export function UpdateAgenda(data, callback) {
  return dispatch => {
    return axios.post(`${process.env.REACT_APP_APIHOST}/v1/updateagenda`, data)
    .then(res => {
      dispatch(setUpdateAgenda(res.data));
      callback('/listagenda');
    })
    .catch(error => {
      dispatch(errorUpdateAgenda(error));
    })
  }
}

