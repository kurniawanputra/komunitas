import axios from 'axios';

export default function setAuthorizationToken(jwt_token) {
  if (jwt_token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${jwt_token}`;
  } else {
    delete axios.defaults.headers.common['Authorization'];
  }
}