import axios from 'axios';
import {
  LOAD_SUCCESS_ADD_USER,
  LOAD_FAIL_ADD_USER,
  LOAD_SUCCESS_UPDATE_USER,
  LOAD_FAIL_UPDATE_USER,
  LOAD_SUCCESS_DELETE_USER,
  LOAD_FAIL_DELETE_USER
} from './../action/types';

export function setAddUser(result) {
  return {
    type: LOAD_SUCCESS_ADD_USER,
    result
  }
}

export function setUpdateUser(result) {
  return {
    type: LOAD_SUCCESS_UPDATE_USER,
    result
  }
}

export function setDeleteUser(result) {
  return {
    type: LOAD_SUCCESS_DELETE_USER,
    result
  }
}

export function errorDeleteUser(error) {
  return {
    type: LOAD_FAIL_DELETE_USER,
    error
  }
}

export function errorAddUser(error) {
  return {
    type: LOAD_FAIL_ADD_USER,
    error
  }
}

export function errorUpdateUser(error) {
  return {
    type: LOAD_FAIL_UPDATE_USER,
    error
  }
}

export function ActionDeleteUser(id_user, callback) {
  return dispatch => {
    return axios.post(`${process.env.REACT_APP_APIHOST}/v1/deleteuser`, id_user)
      .then(res => {
        console.log(id_user);
        dispatch(setDeleteUser(res.data));
      })
      .catch(error => {
        console.log(error);
        dispatch(errorDeleteUser(error));
      })
  }
}

export function AddUser(data, callback) {
  return dispatch => {
    return axios.post(`${process.env.REACT_APP_APIHOST}/v1/adduser`, data)
      .then(res => {
        console.log(data);
        dispatch(setAddUser(res.data));
        callback('/');
      })
      .catch(error => {
        console.log(error);
        dispatch(errorAddUser(error));
      });
  }
}

export function ActionUpdateUser(data, callback) {
  return dispatch => {
    return axios.post(`${process.env.REACT_APP_APIHOST}/v1/updateuser`, data)
      .then(res => {
        console.log(data);
        dispatch(setUpdateUser(res.data));
        callback('/listuser');
      })
      .catch(error => {
        console.log(error);
        dispatch(errorUpdateUser(error));
      })
  }
}